classdef AgentsClass < handle
    % This class represents the agts. The parameters of each agt are
    % defined through the methods.
    
    % Variable list
    % Type - type of agt dynamics
    % T - sampling period
    % (A,B,C,D) - state-space matrices
    % nx,ny,ny - x, y, and u vectors size
    % u_MIN, u_MAX... - bounds
    % P_col, q_col - collision region parameters
    % P_con, q_con - connectivity region parameters
    % conRegVert - vertices of connectivity region
    % Ncs - number o sides collision region
    % Nsc - number of sides connectivity region
    properties
        Ts_plannerLoop = [];
        Ts_posCtrlLoop = [];
        Ts_motorLoop = [];
        A = [];
        B = [];
        C = [];
        D = [];
        nx = [];
        ny = [];
        nu = [];
        u_MIN = [];
        u_MAX = [];
        x_MIN = [];
        x_MAX = [];
        conRegVert = [];
        Nsc = [];
        Ncs = [];
        x = [];
        u = [];
        y = [];
        maxUnforcedDeceleration = [];
        decRegionVertices = [];
        xUnicycle = [];
        vUnicycle = [];
        Ad_MPC = [];
        Bd_MPC = [];
        Cd_MPC = [];
        Dd_MPC = [];
        Ad_SIM = [];
        Bd_SIM = [];
        Cd_SIM = [];
        Dd_SIM = [];
        Ad_60hz = [];
        Bd_60hz = [];
        Cd_60hz = [];
        Dd_60hz = [];
        linVelCommand = [];
        angVelCommand = [];
        linVel = [];
        angVel = [];
        Vert_robotBody = [];
        
        % Robot parameters
        m = [];
        pCenterMass = [];
        wheelRadius = [];
        L = [];
        I = [];
        I_c = [];
        I_w = [];
        I_m = [];
        I_1 = [];
        I_2 = [];
        I_3 = [];
        motor = [];
        posCtrl = [];
        diagnosticsReport = [];
        connRadius = [];
        
        % Encoder
        encoder = [];
        
    end
    
    methods
        %-------------------------------------------------------------------------
        function InitializeVariables(agt,MAS,j,mission)
            % Dynamics
            switch MAS.TYPE(j,:)
                % --------
              
                case 'VSS'
                    % Parameters initialization
                    agt.Ts_plannerLoop =MAS.Ts_plannerLoop;
                    agt.Ts_posCtrlLoop =MAS.Ts_posCtrlLoop;
                    agt.Ts_motorLoop =MAS.Ts_motorLoop;
                    agt.m = MAS.m;
                    agt.pCenterMass = MAS.pCenterMass;
                    agt.wheelRadius = MAS.wheelRadius;
                    agt.connRadius = MAS.BOT_CONNECTIVITY_RADIUS(j);
                    agt.L = MAS.L;
                    agt.I = MAS.I;
                    agt.I_c = MAS.I_c;
                    agt.I_w = MAS.I_w;
                    agt.I_m = MAS.I_m;
                    agt.I_1 = MAS.I_1;
                    agt.I_2 = MAS.I_2;
                    agt.I_3 = MAS.I_3;
                    agt.motor.Kp = MAS.motorCtrl.Kp;
                    agt.motor.Ki = MAS.motorCtrl.Ki;
                    agt.motor.Res = MAS.Res;
                    agt.motor.b = MAS.b;
                    agt.motor.K = MAS.K;
                    agt.motor.N = MAS.N;
                    agt.motor.e = MAS.e;
                    agt.motor.Ac = MAS.motor.Ac;
                    agt.motor.Bc = MAS.motor.Bc;
                    agt.motor.Ad = MAS.motor.Ad;
                    agt.motor.Bd = MAS.motor.Bd;
                    agt.motor.F_sr = MAS.F_sr;
                    agt.motor.F_kr = MAS.F_kr;
                    agt.motor.F_sl = MAS.F_sl;
                    agt.motor.F_kl = MAS.F_kl;
                    agt.motor.alpha_s = MAS.alpha_s;
                    agt.motor.alpha_k = MAS.alpha_k;
                    agt.motor.pastVoltage = 0; %delete this
                    agt.motor.pastWheelVelError = 0;
                    agt.posCtrl.posError_past = 0;
                    agt.posCtrl.u_past = 0;
                    agt.posCtrl.pastdKsi = 0; % ASSUMPTION 2 ON THE PAPER KSI(0) >0 FOR FORWARD MOTION
                    %agt.posCtrl.desiredLinVel = .0075; %initialize to avoid singularity
                    agt.posCtrl.Kp = MAS.posCtrl.Kp;
                    agt.posCtrl.Kd = MAS.posCtrl.Kd;
                    
                    % Robot body set vertices
                    agt.Vert_robotBody = 2*[-agt.L agt.L; agt.L agt.L; agt.L -agt.L; -agt.L -agt.L];
                    
                    % Conn region set vertices
                    R = agt.connRadius;
%                    agt.Vert_connRegion = [-R/2 R/2 R R R/2 -R/2 -R -R;R R R/2 -R/2 -R -R -R/2 R/2]';
                                                                                                                                                                                                                                 
                    
                    
                    % Encoder parameters
                    agt.encoder.nBits = 12;
                    agt.encoder.quantization = 2*pi/agt.encoder.nBits;
                    
                    % Control model (MPC 1Hz)
                    agt.Ad_MPC = [1 agt.Ts_plannerLoop 0 0; 0 1 0 0; 0 0 1 agt.Ts_plannerLoop;0 0 0 1];
                    agt.Bd_MPC = [agt.Ts_plannerLoop^2/2 0; agt.Ts_plannerLoop 0; 0 agt.Ts_plannerLoop^2/2; 0 agt.Ts_plannerLoop];
                    agt.Cd_MPC = [1 0 0 0; 0 0 1 0];
                    agt.Dd_MPC =  zeros(2,2);
                    
                    % Simulation linear model (240Hz)
                    agt.Ad_SIM = MAS.Ad;
                    agt.Bd_SIM = MAS.Bd;
                    agt.Cd_SIM = MAS.Cd;
                    agt.Dd_SIM = MAS.Dd;
                    
                    % Pos control model
                    agt.Ad_60hz = MAS.Ad_60hz;
                    agt.Bd_60hz = MAS.Bd_60hz;
                    agt.Cd_60hz = MAS.Cd_60hz;
                    agt.Dd_60hz = MAS.Dd_60hz;
                    % --------
                otherwise
                    agt.A = [];
                    agt.B = [];
                    agt.C = [];
                    agt.D = [];
            end
            
            % Initial status is nominal
            agt.diagnosticsReport.distressSignal = 0;
            
            % State, control, and output sizes
            agt.nx = size(agt.Ad_SIM,1);
            agt.nu = size(agt.Bd_SIM,2);
            agt.ny = size(agt.Cd_SIM,1);
            
            % State, control, and output variables
            agt.x = zeros(agt.nx,MAS.kfinal_plannerLoop*MAS.kfinal_posCtrlLoop*MAS.kfinal_motorLoop+1);
            agt.xUnicycle = zeros(3,MAS.kfinal_plannerLoop*MAS.kfinal_posCtrlLoop*MAS.kfinal_motorLoop+1);
            agt.vUnicycle = zeros(3,MAS.kfinal_plannerLoop*MAS.kfinal_posCtrlLoop*MAS.kfinal_motorLoop+1);
            agt.u = zeros(agt.nu,MAS.kfinal_posCtrlLoop);
            agt.linVelCommand = zeros(1,MAS.kfinal_plannerLoop*MAS.kfinal_posCtrlLoop+1);
            agt.linVel = zeros(1,MAS.kfinal_plannerLoop*MAS.kfinal_posCtrlLoop*MAS.kfinal_motorLoop+1);
            agt.angVel = zeros(1,MAS.kfinal_plannerLoop*MAS.kfinal_posCtrlLoop*MAS.kfinal_motorLoop+1);
            %  agt.y = zeros(agt.ny,Nmax+1);
            
            % motor state control and output variables
            agt.posCtrl.wheelVelCommand = zeros(agt.nu,MAS.kfinal_plannerLoop*MAS.kfinal_posCtrlLoop);
            agt.motor.voltages = zeros(2,MAS.kfinal_plannerLoop*MAS.kfinal_posCtrlLoop*MAS.kfinal_motorLoop);
            agt.motor.wheelVel = zeros(2,MAS.kfinal_plannerLoop*MAS.kfinal_posCtrlLoop*MAS.kfinal_motorLoop+1);
            agt.motor.quantizedWheelVel = zeros(2,MAS.kfinal_plannerLoop*MAS.kfinal_posCtrlLoop*MAS.kfinal_motorLoop+1);
            %agt.motor.d_eta = zeros(2,MAS.kfinal_plannerLoop*MAS.kfinal_posCtrlLoop*MAS.kfinal_motorLoop+1);
            agt.motor.quantizedAxisVel = zeros(2,MAS.kfinal_plannerLoop*MAS.kfinal_posCtrlLoop*MAS.kfinal_motorLoop+1);
            agt.motor.quantizedAxisAngles = zeros(2,MAS.kfinal_plannerLoop*MAS.kfinal_posCtrlLoop*MAS.kfinal_motorLoop+1);
            agt.motor.angles = zeros(2,MAS.kfinal_plannerLoop*MAS.kfinal_posCtrlLoop*MAS.kfinal_motorLoop+1);
            
            % Initial condition initialization
            agt.x(:,1) = mission.x0(:,j);
            agt.xUnicycle(1:2,1) = [1 0 0 0;0 0 1 0]*mission.x0(:,j);
            agt.vUnicycle(1,1) = mission.x0(2,j); %vx
            agt.vUnicycle(2,1) = mission.x0(4,j); %vy
            
            
            % Bounds
            agt.x_MIN = MAS.x_MIN{j};
            agt.x_MAX = MAS.x_MAX{j};
            agt.u_MIN = MAS.u_MIN{j};
            agt.u_MAX = MAS.u_MAX{j};
            
            agt.Ncs = 4;
            
            % max unforced dec
            agt.maxUnforcedDeceleration = agt.x_MAX(2);
            
            % Connectivity region
            switch MAS.BOT_CONN_REGION_SHAPE(j,:)
                case 'SQUARE'
                    %                     agt.P_con = [eye(agt.nx); -eye(agt.ny)];
                    %                     agt.q_con = MAS.BOT_CONNECTIVITY_RADIUS(j)*ones(4,1);
                    agt.conRegVert = MAS.BOT_CONNECTIVITY_RADIUS(j)*[1 1;-1 1;-1 -1;1 -1];
                    
                case 'OCTAGON'
                    %                     agt.P_con = [eye(agt.ny); -eye(agt.ny); [-1 1;1 1;1 -1;-1 -1] ];
                    %                     agt.q_con = [MAS.BOT_CONNECTIVITY_RADIUS(j)*ones(4,1); MAS.BOT_CONNECTIVITY_RADIUS(j)*ones(4,1)];
                    agt.conRegVert = MAS.BOT_CONNECTIVITY_RADIUS(j)*[1  0.5; 0.5  1;-0.5  1;-1  0.5;
                        -1 -0.5;-0.5 -1; 0.5 -1; 1 -0.5];
                    
            end
            agt.Nsc = length(agt.conRegVert);
        end
        %-------------------------------------------------------------------------
        function motorVoltage_k = MotorController(agt,axisVel_k,desAxisVel_k)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method implements a discrete PI controller. The
            % equations are derived from tustin discretization
            % approximation. The controlled variable is the motor angular
            % velocity, without the reduction.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % Motor angular vel. error
            wheelVelError = desAxisVel_k - axisVel_k;
            
            % Motor PI controller (discretized with Tustin)
            % V(k) = V(k-1) + (Ki*(T/2)+Kp)*e(k) + (Ki*(T/2) - Kp)*e(k-1)
            % this was double checked, move on if debugging
            motorVoltage_k = agt.motor.pastVoltage + (agt.motor.Ki*(agt.Ts_motorLoop/2)+agt.motor.Kp)*wheelVelError...
                + (agt.motor.Ki*(agt.Ts_motorLoop/2)-agt.motor.Kp)*agt.motor.pastWheelVelError;
            
            % Control saturation
            % double checked, move on if debugging
                        if motorVoltage_k(1)> 8.3;  motorVoltage_k(1) = 8.3; end
                        if motorVoltage_k(1)< -8.3; motorVoltage_k(1) = -8.3; end
                        if motorVoltage_k(2)> 8.3;  motorVoltage_k(2)  = 8.3; end
                        if motorVoltage_k(2)< -8.3; motorVoltage_k(2)  = -8.3; end
            
            % saves last error for next control law calculation
            agt.motor.pastWheelVelError = wheelVelError;
            agt.motor.pastVoltage = motorVoltage_k;
            
        end
        %-------------------------------------------------------------------------
        function [quantizedAxisAngle_next,quantizedAxisVel_next] = encoderQuantization(agt,nPulses,lastQuantizedAngle)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method implements the quantization process. It receives%
            % the number of pulses counted by the encoder, quantizes it,  %
            % and then calculates the quantized axis velocity.            %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            quantizedAxisAngle_next = nPulses*agt.encoder.quantization;
            quantizedAxisVel_next = (quantizedAxisAngle_next  - lastQuantizedAngle)/agt.Ts_motorLoop; % quantized ang vel.
            
        end
        %-------------------------------------------------------------------------
        function [nPulses,angles_next] = encoderDynamics(agt,wheelVel,angles_k,k)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % The objective of this method is to emulate an encoder. In   %
            % order to do that the angular velocity (output of the motor  %
            % dynamics) is integrated (trapezoidal). Then, the number of  %
            % pulses is determined by "reverse quantization".             %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % The model outputs the velocity of the wheel (with reduction).
            % However the encoder is mounted in the motor axis and this
            % velocity must be converted through gear ratio.
            % It is often better to mount the encoder in the motor axis,
            % since it is 'N' times faster than the wheel, resulting in
            % better encoder readings (less time stuck between quantization
            % levels results in smoother derivatives, I guess).
            axisVel = wheelVel*agt.motor.N;
            
            % w(k) = w(k-1) + (e(k) + e(k-1))(T/2)
            % Determine angle through numeric integration
            angles_next = angles_k + (axisVel(:,k+1)+ axisVel(:,k))*(agt.Ts_motorLoop/2);
            
            % Convert angle to number of pulses (inverse quantization)
            nPulses = floor(angles_next/(agt.encoder.quantization));
            
        end

        %-------------------------------------------------------------------------
        function next_state = FullRobotDynamics(agt,t,x_k,u_k)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method implements the FULL continuous-time dynamics of %
            % robot. The model is the nonlinear unicycle kinematics and   %
            % LTI motor+gearBox+robotBody dynamics.                       %
            % The variables are:                                          %
            % x_k = [rx;ry;theta,wheelVel_R,wheelVel_L];                  %
            % u_k = [motorVoltage;motorVoltage_L];                        %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % Friction model (static - kinetic)
            f_k = [agt.motor.F_sr*tanh(agt.motor.alpha_s*x_k(4))-agt.motor.F_kr*tanh(agt.motor.alpha_k*x_k(4));
                agt.motor.F_sl*tanh(agt.motor.alpha_s*x_k(5))-agt.motor.F_kl*tanh(agt.motor.alpha_k*x_k(5))];
            
            
            next_state = [
                (agt.wheelRadius/2)*cos(x_k(3))*x_k(4) + (agt.wheelRadius/2)*cos(x_k(3))*x_k(5);
                (agt.wheelRadius/2)*sin(x_k(3))*x_k(4) + (agt.wheelRadius/2)*sin(x_k(3))*x_k(5);
                agt.wheelRadius/(2*agt.L)*x_k(4) - agt.wheelRadius/(2*agt.L)*x_k(5);
                agt.motor.Ac(1,1)*x_k(4) + agt.motor.Ac(1,2)*x_k(5);
                agt.motor.Ac(2,1)*x_k(4) + agt.motor.Ac(2,2)*x_k(5)]...
                + ...
                [0, 0;
                0, 0;
                0, 0;
                agt.motor.Bc(1,1), agt.motor.Bc(1,2);
                agt.motor.Bc(2,1), agt.motor.Bc(2,2)]*u_k + [0;0;0;f_k];
            
            
        end
        
        %-------------------------------------------------------------------------
        function [next_state,next_output] = LinearRobotDynamics(agt,x_k,acc,failedAgentIndex,oldIndex,newIndex)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Double integrator dynamics.
            % It receives the oldIndex to known switch the dynamics of the
            % failed agent. At the same time it used the newIndex to assign
            % the correct control value to the functional agents. Since
            % there is a "choice" of input, it also outputs the applied input.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            if oldIndex == failedAgentIndex
                % Failure input
                u_k = zeros(2,1);
                x_k = diag([1,0.5,1,0.5])*x_k;
            else
                u_k = acc(:,1,newIndex); % apply first acc from the sequence (MPC)
            end
            
            next_state = agt.A*x_k + agt.B*u_k;
            next_output = agt.C*next_state;
            % current_input = u_k;
            
        end
        
        %-------------------------------------------------------------------------
        function [linVelCommand_next,angVelCommand] = TrackingController(agt,...
                x_j,v_j,linVelCommand,posCommand_i,velCommand_i,accCommand_i,j)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method implements the tracking controller. It receives
            % the commands from the MPC and outputs wheel velocity commands
            % to the motor control law.
            %
            % The objective is to determine how the robot's wheels must
            % respond such that the robot follows the trajectory provided
            % by the MPC. The problem is that the MPC uses a double
            % integrator model, while the robot behaves as an unicycle
            % model. Therefore, we utilize the compensator proposed in
            %
            % STABILIZATION OF THE UNICYCLE VIA DYNAMIC FEEDBACK LINEARIZATION
            % De Luca et. al (2000)
            %
            % to build a closed-loop such that the resulting dynamics of
            % the real robot is approximately equal to a double integrator.
            % The output of this control law are wheel velocities (which
            % are the inputs of the unicycle model). A internal motor
            % controller will then track these wheel velicity commands.
            %
            % IMPORTANT: there's a singularity in this compensator when the
            % linear velocity is equal to zero.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % Extracts position and angle from state vector
            r_j = x_j(1:2);
            theta_j = x_j(3);
            
            % PD + feedforward control law
            u_k = accCommand_i + agt.posCtrl.Kp*(posCommand_i - r_j) + agt.posCtrl.Kd*(velCommand_i - v_j);
            
            % Feedback linearization compensator see comments above
            dKsi_j = u_k(1)*cos(theta_j) + u_k(2)*sin(theta_j);
            linVelCommand_next = linVelCommand + (dKsi_j + agt.posCtrl.pastdKsi)*(agt.Ts_posCtrlLoop/2);
            angVelCommand = (u_k(2)*cos(theta_j)-u_k(1)*sin(theta_j))/linVelCommand;

            % Saves internal variables for posterior integration
            agt.posCtrl.pastdKsi = dKsi_j;
        end
        %-------------------------------------------------------------------------
        function wheelVelCommand = ComputeWheelVelCommand(agt,linVel,angVel)
            
            % Linear mapping between desired linear and angular
            % velocities and wheel velocities (omega_R/omega_L)
            omega_R = (linVel + angVel*agt.L)/(agt.wheelRadius);
            omega_L = (linVel - angVel*agt.L)/(agt.wheelRadius);
            
            wheelVelCommand = [omega_R;omega_L];
        end
        %-------------------------------------------------------------------------
        
        function diagnosticsReport = SendReport(agt,failInstant,currentStep)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method emulates a status message sent by the robot     %
            % communicating its current status. For now it informs about  %
            % the occurence of some failure.                              %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % Determines the kth instant in which the failure occurs
            % failureStep = failInstant/agt.Ts_motorLoop;
            failureStep = failInstant/agt.Ts_plannerLoop;
            diagnosticsReport.distressSignal = 0;
            
            % Switch distress signal status
            if currentStep >= failureStep  && failureStep ~= 0; diagnosticsReport.distressSignal = 1; end
            
        end
        %-------------------------------------------------------------------------
        function [achievedLinVel,achievedAngVel] = ComputeRealUnicycleVel(agt,realWheelVel)
                        % Linear mapping between desired linear and angular
            % velocities and wheel velocities (omega_R/omega_L)
            achievedLinVel = (agt.wheelRadius/2)*(realWheelVel(1) + realWheelVel(2));
            achievedAngVel = (agt.wheelRadius)/(2*agt.L)*(realWheelVel(1) - realWheelVel(2));

            
        end
        %-------------------------------------------------------------------------
    end
end

