classdef DataProcessorClass < handle
    % This class handles all data processing, such as
    % reorganization and saving.
    
    % Variable list
    % MPCSolutions - this structure contains the MPC solutions for each 'k'
    % terminalStep_i - mission ending instant
    % wasTargetVisited - vector which specifies if a target was visited
    % lambdaPast - previous task allocation vector
    % totalTaskChanges - total number of task changes during mission
    
    properties
        MPCSolutions = [];
        MPCIniSolution = [];
        terminalStep_i = [];
        terminalStep_k = [];
        wasTargetVisited = [];
        lambdaPast = [];
        totalTaskChanges = [];
        new2oldIndex = [];
        kFail = [];
        failedAgentIndex  = [];
        failureTimeStep = [];
        Na_orig = [];
    end
    
    methods
        % -------------------------------------------------------------------------
        function InitializeVariables(dp,mission,MPC,failureInfo)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method initializes general variables that will be
            % used in the remaining methods.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % auxiliary variables
            dp.wasTargetVisited = zeros(1,mission.Nt);
            dp.lambdaPast = zeros(1,mission.Na*mission.Nt);
            dp.new2oldIndex = [];
            dp.kFail = sum(failureInfo);
            dp.Na_orig = mission.Na;
            dp.failedAgentIndex = 0;
        end
        % -------------------------------------------------------------------------
        function optHor = FindOptimalHorizont(dp)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method finds the optimal horizont N* for the
            % optimization at each instant.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            optHor = zeros(1,dp.terminalStep_i-1);
            
            % minus one because to avoid counting initial condition
            for k=1:dp.terminalStep_i-1 % no optimization at terminal k
                optHor(k) = find(dp.MPCSolutions(k).B_hor == 1)-1;
            end
        end
        % -------------------------------------------------------------------------
        function [nBinaryVar,nContinuousVar] = FindNumberOptVar(dp,MPC)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method finds the total number of continuous and binary
            % variables in the optimization problem.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%            nContinuousVar = length([MPC.X;MPC.U;MPC.Y;MPC.abs_U]);
             nContinuousVar = length([MPC.X;MPC.U;MPC.abs_U]);
            nBinaryVar = length([MPC.B_hor;MPC.B_target;MPC.B_col;MPC.B_con;MPC.B_obs]);
        end
  
        % -------------------------------------------------------------------------
        function [graphsPhys,graphsPhysLOS] = BuildConnectivityGraphs(dp,agent,mission,env,LOSflag,MPC)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method builds the communication graphs based on the
            % position of the agents (real) or MPC solutions (virtual).
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % Preliminaries
            %graphs_opt = cell(dp.terminalStep_i,1);
            graphsPhys = cell(dp.terminalStep_i,1); % cells that receives graph obj
            isConn = zeros(mission.Na,mission.Na-1,dp.terminalStep_i);
            graphsPhysLOS = graphsPhys;
            isConnLOS = isConn;

            % Creates graph based on physical proximity
            for k=1:dp.terminalStep_i
                
                % add Na vertices
                G_phys = graph; G_phys = addnode(G_phys,mission.Na);
                
                % Case LOS
                if LOSflag 
                    % add Na vertices
                    G_physLOS = graph; G_physLOS = addnode(G_physLOS ,mission.Na);
                else
                    G_physLOS = [];
                end
                
                % Add vertices
                for j=1:mission.Na-1
                    for i=j+1:mission.Na
                        r_i = [agent(i).x(1,k);agent(i).x(3,k)];
                        r_j = [agent(j).x(1,k);agent(j).x(3,k)];
                        coordDiff = r_i - r_j;

                        % if there is at least 1 zero -> not in conn. reg.
                        % This is the connectivity region requisite
                        isConn(j,i,k) = min(MPC.connSet(j).P(:,:,1)*coordDiff <=...
                            MPC.connSet(j).q(:,1) + 0.001); % gotta add this epsilon to avoid 1.0000071 <= 1
                        
                        % If LOS must be considered and there is connection
                        % due to proximity, investigate the existe of
                        % line of sight. Here I calculate samples of the
                        % line of sight and check if they violate any obs.
                        % Starts by atributing the same value
                        isConnLOS(j,i,k) = isConn(j,i,k);
                        if LOSflag && isConn(j,i,k)
                            alpha = [0:0.01:1]; % samples see convex combination of two points
                            for n=1:length(alpha)
                                % This calculates the samples in the line
                                lineOfSightSample = alpha(n)*r_i + (1-alpha(n))*r_j;
                                % Checks for each obstacle if the sample is
                                % inside obstacle
                                for t=1:env.No
                                    isThereLOS = ~all(env.P_obs{t}*lineOfSightSample <= env.q_obs{t});
                                    if ~isThereLOS
                                        isConnLOS(j,i,k) = 0; 
                                        break; 
                                    end %stops for if there is no LOS
                                end
                                if ~isThereLOS;break; %stops for if there is no LOS                                  
                                end
                            end
                        end % end LOS conditional
                        
                        % Remove failed robot connections

                       if ~dp.failureTimeStep % needed when dp.failureTimeStep = [];
                            if k>0 && k >= dp.failureTimeStep
                                if j == dp.failedAgentIndex || i == dp.failedAgentIndex
                                    isConn(j,i,k) = 0;
                                end
                            end
                       end
                        
                        % Add edges
                        if isConn(j,i,k); G_phys = addedge(G_phys,j,i); end
                        if LOSflag && isConnLOS(j,i,k);  G_physLOS = addedge(G_physLOS,j,i); end
                    end
                end
                graphsPhys{k} = G_phys;
                graphsPhysLOS{k} = G_physLOS;
            end
        end
   
        % -------------------------------------------------------------------------
        function isNetworkRobust = CheckNetworkRobustness(dp,xAll_k,agent,MPC,mission)
            
            % Pre-allocation
            isRobConn = zeros(mission.dynNa);
            
            % parte do acochambramento, como o xAll_k ainda contem os
            % estados do agente falho, tem que tirar manualmente
            if dp.failedAgentIndex>0; xAll_k(:,dp.failedAgentIndex) = []; end
            
            % Constructs degrees
            for j=1:mission.dynNa-1
                for i=j+1:mission.dynNa
                    coordDiff = [xAll_k(1,j)-xAll_k(1,i);
                                 xAll_k(3,j)-xAll_k(3,i)];
                    
                    % if there is at least 1 zero -> not in conn. reg.
                     isRobConn(j,i) = min(MPC.connSet(j).P(:,:,1)*coordDiff <=...
                         MPC.connSet(j).q(:,1) + 0.001); % gotta add this epsilon to avoid 1.0000071 <= 1
                    isRobConn(i,j) = isRobConn(j,i); %adjacency matrix is symmetric
                end
            end
            
            % Sum columns for degree of each vertex
            degrees = sum(isRobConn,2);
            
            % Checks connectivity depending on approach
            switch MPC.CONNECTIVITY_CONSTRAINT_TYPE
                case 'ROBUST_MINVERTICEDEGREE'
                    if failedAgendIndex; degrees(dp.failedAgentIndex,:) = [];end % removes line of failed agent
                    isNetworkRobust = min(degrees >= mission.dynNa/2);
                    %
                case 'ROBUST_SUMVERTICEDEGREE'
                    % if this loop runs without interruption the network is
                    % robust according to this constraint. Otherwise it
                    % will get isNetworkRobust = 0;
                    isNetworkRobust = 1;
                    for j=1:mission.dynNa-1
                        for i=j+1:mission.dynNa
                            const_ji = min(degrees(j) + degrees(i) >= mission.dynNa - isRobConn(j,i)*mission.dynNa);
                            if const_ji == 0
                                isNetworkRobust = 0; break;
                            end
                        end
                        if const_ji ==0; break; end % ends external loop
                    end
            end
        end
        
        % -------------------------------------------------------------------------
        function [nCutVertices,avgCutVertices,nCutVerticesLOS,avgCutVerticesLOS] = ...
                FindNumberCutVertex(dp,graph,graphLOS,mission,LOSflag)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Calculates number of cut vertices and average number of cut
            % vertices. To do that we simply remove one vertice at time and
            % see the amount of components is different from 1.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            

            
            isCutVertex = zeros(mission.Na,dp.terminalStep_i);
            for k=1:dp.terminalStep_i
                
                for j=1:mission.Na
                    % Ignore failed agent
                    if j==dp.failedAgentIndex &&k>=dp.failureTimeStep;  continue; end
                    
                    % always refresh Gk after removing vertex
                    Gk = graph{k};
                    
                    % Ignore connections to the failed agent 
                    if k>=dp.failureTimeStep 
                        Gk = rmnode(Gk,dp.failedAgentIndex);
                    end
                    
                    % removes jth vertex
                    Gk = rmnode(Gk,j);
                    
                    % if there are more than 1 component jth is cut vertex
                    if max(conncomp(Gk)) > 1; isCutVertex(j,k) = 1; end
                end
            end
            
            % sum columns to determine number of cut vertices
            nCutVertices = sum(isCutVertex);
            avgCutVertices = sum(nCutVertices)/dp.terminalStep_i;
            
            % repeats process for LOS case
            if LOSflag
            isCutVertex = zeros(mission.Na,dp.terminalStep_i);
            for k=1:dp.terminalStep_i
                
                for j=1:mission.Na
                    % Ignore failed agent
                    if j==dp.failedAgentIndex &&k>=dp.failureTimeStep;  continue; end
                    
                    % always refresh Gk after removing vertex
                    Gk = graphLOS{k};
                    
                    % Ignore connections to the failed agent 
                    if k>=dp.failureTimeStep 
                        Gk = rmnode(Gk,dp.failedAgentIndex);
                    end
                    
                    % removes jth vertex
                    Gk = rmnode(Gk,j);
                    
                    % if there are more than 1 component jth is cut vertex
                    if max(conncomp(Gk)) > 1; isCutVertex(j,k) = 1; end
                end
            end 
            nCutVerticesLOS = sum(isCutVertex);
            avgCutVerticesLOS = sum(nCutVerticesLOS)/dp.terminalStep_i;
            
            else
            nCutVerticesLOS = nCutVertices; avgCutVerticesLOS=avgCutVertices;
            end
            
        end
        % -------------------------------------------------------------------------
        function missionData = CompactMissionData(dp,agent,MAS,env,optHorizonts,...
                fixedHorizont,mission,solverOptions,missionObjectiveValue,...
                nVisTargets,nBinVar,nContVar,nCutVert,avgCutVert,nCutVerticesLOS,avgCutVerticesLOS,......
                graphsPhys,graphsPhysWithLOS,...
                nConst,connConstType,dynIndex,kFail,failedAgentIndex,i,j,k)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method is used to create a data structure with the
            % mission data. This compaction is helpful in the displayer
            % class.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            missionData.agent = agent;
            missionData.MAS = MAS;
            missionData.env = env;
            missionData.optSolutions = dp.MPCSolutions;
            missionData.N_opt = optHorizonts;
            missionData.N = fixedHorizont;
            missionData.solverOptions = solverOptions;
            missionData.mission = mission;
            missionData.missionObj = missionObjectiveValue;
            missionData.nVisitedTargets = nVisTargets;
            missionData.nBinaryVar = nBinVar;
            missionData.nContinuousVar = nContVar;
            missionData.nCutVertices = nCutVert;
            missionData.avgCutVertices = avgCutVert;
            missionData.nCutVerticesLOS = nCutVerticesLOS;
            missionData.avgCutVerticesLOS = avgCutVerticesLOS;
            missionData.graphsPhys = graphsPhys;
            missionData.graphsPhysWithLOS = graphsPhysWithLOS;
            missionData.nConst = nConst;
            missionData.connConstType = connConstType;
            missionData.solverTimeLimit = solverOptions.gurobi.TimeLimit;
            missionData.Na_orig = dp.Na_orig;
            missionData.dynIndex = dynIndex;
            missionData.kFail= kFail;
            missionData.failedAgentIndex = failedAgentIndex;
            missionData.terminalStep_i = i;
            missionData.terminalStep_j = j;
            missionData.terminalStep_k = k;
            
            
        end
        % -------------------------------------------------------------------------
        function missionObjectiveValue = FindMissionObjective(dp,agent,nVisTargets,MPC)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method calculates the value of the real objective(cost),
            % i.e., the objective using the real mission data.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            sumNormU = 0;
            for k=1:dp.terminalStep_i-1
                for i=1:length(agent)
                    sumNormU = sumNormU + norm(agent(i).u(:,k),1);
                end
            end
            missionObjectiveValue = dp.terminalStep_i + MPC.gamma*sumNormU ...
                - MPC.targetReward(1)*(nVisTargets-1); %*nVisTar-1 bcause i dont count the last one
        end
  
        function  SaveDataToMachine(dp,missionData)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method saves a few variables. It would be nice to
            % automate the filename to have some stamp of the day and time
            % in which this particular data was generated. This way
            % avoiding overwrites and organizing the results.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            time = missionData.solverOptions.gurobi.TimeLimit;
            folder = pwd+"\Results";
            filename = "RobustnessRecoveryPaper_"+time+"s_"+missionData.connConstType;
            save(fullfile(folder,filename),'missionData');
        end
        % -------------------------------------------------------------------------
        function SaveMPCSolutions(dp,sol,k)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method saves only the solutions of the MPC at each
            % instant.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            dp.MPCSolutions(k).X = sol{1};
            dp.MPCSolutions(k).U = sol{2};
            dp.MPCSolutions(k).abs_U = sol{3};
            dp.MPCSolutions(k).B_hor = sol{4};
            dp.MPCSolutions(k).B_obs = sol{5};
            dp.MPCSolutions(k).B_target = sol{6};
            dp.MPCSolutions(k).B_col = sol{7};
            dp.MPCSolutions(k).B_con = sol{8};
            dp.MPCSolutions(k).B_samp = sol{9};
        end
        % -------------------------------------------------------------------------
        function  BuildTaskAllocationVector(dp,mission,MPC,k)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method builds and saves the task allocation vector at
            % each instant. This information is then used in the cost to
            % penalize certain changes in the task allocation.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % Preliminaries
            aux = 0;
            dp.lambdaPast = zeros(1,mission.Na*mission.Nt);
            for j=1:mission.Na
                for t=1:mission.Nt
                    aux = aux+1;
                    dp.lambdaPast(aux) = sum(dp.MPCSolutions(k).B_target(MPC.B_targetIndex(t,:,j)));
                end
            end
        end
        % -------------------------------------------------------------------------
        function error = ComputeTrajectoryError(dp,agent,traj)

             %end_i = length(agent(1).posCtrl.posCommand);
             end_i = (traj.optimalHorizon-1)*60;
             for i=1:end_i
                 if i==1; index = 1; else; index=(i-1)*4;end
               x_k = [agent(1).x(1,index);agent(1).x(3,index)];
               error(:,i) = agent(1).posCtrl.posCommand(:,i)-x_k;
               if i == 98; a=1; end;
            end
            
            
        end
        
        
    end
end