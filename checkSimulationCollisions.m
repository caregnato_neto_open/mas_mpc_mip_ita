function collisions = checkSimulationCollisions(agent,MPC)


tempInd = find(agent(1).x(1,:)==0 & agent(1).x(3,:)==0);

tempInd = tempInd(1);

if (agent(2).x(1,tempInd) == 0 && agent(2).x(3,tempInd)==0)
    lastTimeStep = tempInd;
end

nRobots = length(agent);


% Redeclare polytope for better readibility
%P = MPC.colSet(1).P(:,:,1);
P = [eye(2);-eye(2)];
%q = MPC.colSet(1).q(:,1);

% original robot size 
robotLength = 0.074;
q = robotLength*ones(4,1);

% Extract positions from state vector
C = [1 0 0 0;0 0 1 0];


% Collision table initialization
collisions = zeros(nRobots,nRobots,lastTimeStep);


for i=1:nRobots-1
    
    for j=i+1:nRobots
        
        for k=1:lastTimeStep

            
            posDiff = C*(agent(i).x(:,k) - agent(j).x(:,k));
            if sum(-P*posDiff <= -q) < 1
                collisions(i,j,k) = 1;
                break;
            end
            
            
            
        end
        
    end
    
    
    
    
end