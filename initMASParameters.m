function MAS = initMASParameters(mission,env,agentType)

switch agentType
 
        
        % ----------------------------- VSS -----------------------------------
    case 'VSS'
        % Time parameters
        % Planner loop: 1Hz
        % Sampling
        MAS.Ts_plannerLoop = 1;
        MAS.kfinal_plannerLoop = mission.ETA_s/MAS.Ts_plannerLoop;
        MAS.Ts_posCtrlLoop = 1/60; % Pos control loop: 60Hz (camera)
        MAS.kfinal_posCtrlLoop = MAS.Ts_plannerLoop/MAS.Ts_posCtrlLoop;
        MAS.Ts_motorLoop = 1/240;  % Sampling: 240Hz
        MAS.kfinal_motorLoop = MAS.Ts_posCtrlLoop/MAS.Ts_motorLoop;
        
        % Communication depends on line of sight?
        MAS.LOS =1;
        
        % Model parameters-------------------------------------
        % General stuff
        MAS.TYPE = repmat("VSS",[mission.Na,1]); % dynamics
        
               maxAcc = 0.75*MAS.Ts_plannerLoop; minAcc = -0.75*MAS.Ts_plannerLoop;
        maxVel = 0.75*MAS.Ts_plannerLoop; minVel = -0.75*MAS.Ts_plannerLoop;



        for i=1:mission.Na
            MAS.u_MIN{i} = minAcc*ones(2,1);
            MAS.u_MAX{i} = maxAcc*ones(2,1);
            MAS.x_MIN{i} = [env.opRegionCenter(1)-env.opRegionLength/2; minVel; env.opRegionCenter(2)-env.opRegionWidth/2; minVel];
            MAS.x_MAX{i} = [env.opRegionCenter(1)+env.opRegionLength/2; maxVel; env.opRegionCenter(2)+env.opRegionWidth/2; maxVel];
        end
        
        % General parameters
        MAS.m = 300.3e-3; % mass
        MAS.length = 0.074;
        %MAS.length = 0.11;
        
        
        MAS.pCenterMass = [0 0]; % position of the center of mass of the robot
        MAS.wheelRadius = 0.03; % R on okuyama (2017)
        MAS.L = MAS.length/2; % half robot's length
        MAS.BOT_DIMENSIONS = repmat([MAS.length MAS.length 0.07],[mission.Na,1]);
        MAS.BOT_CONNECTIVITY_RADIUS = 0.5*ones(mission.Na,1);
        MAS.BOT_CONN_REGION_SHAPE = repmat('OCTAGON',[mission.Na,1]);
        MAS.I = (MAS.m*MAS.length^2)/6; % Uniform robot's moment of inertia
        MAS.I_c = MAS.m*(MAS.pCenterMass(1)^2 + MAS.pCenterMass(2)^2) + MAS.I; % Adjusts moment of inertia if center of mass is different from zero
        MAS.I_w = 8.1e-5;    % moment of inertia of the wheel
        MAS.I_m = 2.1321e-8; % moment of inertia of the motor
        MAS.I_1 = MAS.I_w + MAS.wheelRadius^2*(MAS.I_c - 2*MAS.m*MAS.L*MAS.pCenterMass(2) + MAS.m*MAS.L^2)/(4*MAS.L^2);
        MAS.I_2 = MAS.wheelRadius^2*(MAS.m*MAS.L^2 - MAS.I_c)/(4*MAS.L^2);
        MAS.I_3 = MAS.I_w + MAS.wheelRadius^2*(MAS.I_c + 2*MAS.m*MAS.L*MAS.pCenterMass(2) + MAS.m*MAS.L^2)/(4*MAS.L^2);
        MAS.Res = 4.5; % circuit resistante: Okuyama(2015)=4.5ohms, pololu=3.75
        MAS.b = 5.8734e-8; % viscous friction
        MAS.K = 0.0016;  % motor torque constant
        MAS.N = 51.45;   % gear ratio
        MAS.e =  0.7805; % transmission efficiency
        
        % Matrices for motor dynamics(see okuyama et al.(2017))
        barH_m = [MAS.I_1 + MAS.N^2*MAS.I_m, MAS.I_2;
            MAS.I_2          , MAS.I_3 + MAS.N^2*MAS.I_m];
        
        barC_m = [MAS.e*MAS.N^2*(MAS.K^2/MAS.Res + MAS.b), 0               % considering center of mass = [0;0],
            0,                   MAS.e*MAS.N^2*(MAS.K^2/MAS.Res + MAS.b)]; % then this diagonal is zero
        
        barB_m = [MAS.e*MAS.N*MAS.K/MAS.Res, 0;
            0        , MAS.e*MAS.N*MAS.K/MAS.Res];
        
        % State space matrices for motor dynamics
        MAS.motor.Ac = -barH_m\barC_m;
        MAS.motor.Bc = barH_m\barB_m;
        MAS.motor.Cc = eye(2);
        MAS.motor.Dc = zeros(2);
        [MAS.motor.Ad, MAS.motor.Bd, MAS.motor.Cd,MAS.motor.Dd] =...
            c2dm(MAS.motor.Ac,MAS.motor.Bc,MAS.motor.Cc,MAS.motor.Dc,MAS.Ts_motorLoop,'zoh');
        
        % Control model (double integrator)
        MAS.A = [0 1 0 0;0 0 0 0;0 0 0 1;0 0 0 0];
        MAS.B = [0 0;1 0;0 0;0 1];
        MAS.C = [1 0 0 0;0 0 1 0];
        MAS.D = eye(2);
        [MAS.Ad, MAS.Bd, MAS.Cd,MAS.Dd] = c2dm(MAS.A,MAS.B,MAS.C,MAS.D,MAS.Ts_motorLoop,'zoh');
        [MAS.Ad_60hz, MAS.Bd_60hz, MAS.Cd_60hz,MAS.Dd_60hz] = c2dm(MAS.A,MAS.B,MAS.C,MAS.D,MAS.Ts_posCtrlLoop,'zoh');
        
        % Friction model
        % This are parameters for the friction model, which models
        % static and kinetic friction. Viscous friction is modeled
        % by the parameter 'b'. They are all from (okuyama 2017)
        MAS.F_sr = 0.75;
        MAS.F_kr = 0.41;
        MAS.F_sl = 0.7;
        MAS.F_kl = 0.21;
        MAS.alpha_s = 5.19;
        MAS.alpha_k = 0.39;
        
        % Motor controller
        MAS.motorCtrl.Kp = 0.0043;
        MAS.motorCtrl.Ki = 0.1981;
        
%         % Position controller
         MAS.posCtrl.Kd = diag([3 3]);
         MAS.posCtrl.Kp = diag([2 2]);

        
        
end % End switch

end


