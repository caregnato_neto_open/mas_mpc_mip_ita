function solverOptions = initSolverParameters(solvername)
% In this function one can modify solver parameters.

solverOptions = sdpsettings;

% Show optimization status after each closed-loop iteration
solverOptions.showRealTimeDiagnostics = 1; % dont comment/remove this

switch solvername
    case 'gurobi'
        
        % General options
         solverOptions.verbose = 1;
         solverOptions.solver = solvername; % determiens solver
         solverOptions.gurobi.TimeLimit = 5;
         solverOptions.gurobi.FeasibilityTol =1e-03;
         solverOptions.gurobi.Method = 0;
         solverOptions.gurobi.SimplexPricing = 3;
         solverOptions.gurobi.Presolve = 1;
         solverOptions.usex0 = 1;

        % Determines what is printed after mission
        solverOptions.report = [];
        
        
end

