classdef ConstraintTighteningClass < handle
    
    properties
        W = [];
        N = [];
        L = [];
        
        % sets
        trgSet = [];
        obsSet = [];
        colSet = [];
        
        % temp
        polyTrgSet = [];
        polyObsSet = [];
        polyColSet = [];
        polyConnSet = [];
        polyStateSet = [];
        polyInputSet = [];
        polyPositionSet = [];
        polyVelocitySet = [];
        % options
        lProgOptions = [];
    end
    
    methods
        function obj = ConstraintTighteningClass(distSet,mpc,mission,agent,env)
            obj.W = distSet;
            obj.N = mpc.N;
            obj.lProgOptions = optimoptions('linprog','Display','none');

            % initialize nominal sets
            Na = length(agent);                        
            

            
            obj.polyInputSet(1).P(:,:,1) = [eye(agent(1).nu);-eye(agent(1).nu)];
                obj.polyInputSet(1).q(:,1) = [agent(1).u_MAX;-agent(1).u_MIN];
            
            % Loops 1 to Na (no combinations)
            for i=1:Na
                for k=1:obj.N+1
                    
                    % When using the field shaped position constraint, one
                    % must separate position and velocity polytopes
                    
                    upperRightDiagLineParam = inv([(env.fieldLength/2-env.fieldDiagLength) 1;
                        env.fieldLength/2 1])*[env.fieldWidth/2;env.fieldWidth/2-env.fieldDiagWidth];
                    
                    lowerRightDiagLineParam = inv([(env.fieldLength/2-env.fieldDiagLength) 1;
                        env.fieldLength/2 1])*-[env.fieldWidth/2;env.fieldWidth/2-env.fieldDiagWidth] ;
                    
                    lowerLeftDiagLineParam = inv([-(env.fieldLength/2-env.fieldDiagLength) 1;
                        -env.fieldLength/2 1])*-[env.fieldWidth/2;env.fieldWidth/2-env.fieldDiagWidth] ;
                    
                    upperLeftDiagLineParam = inv([-(env.fieldLength/2-env.fieldDiagLength) 1;
                        -env.fieldLength/2 1])*[env.fieldWidth/2;env.fieldWidth/2-env.fieldDiagWidth]  ;
                    
                    obj.polyPositionSet.P{i,k} = [ eye(2);
                        -eye(2);
                        1 upperRightDiagLineParam(1);
                        -1 -lowerRightDiagLineParam(1);
                        -1 -lowerLeftDiagLineParam(1);
                        1 upperLeftDiagLineParam(1)];
                    

                     q_nom =   [env.fieldLength/2;
                        env.fieldWidth/2;
                        env.fieldLength/2;
                        env.fieldWidth/2
                        upperRightDiagLineParam(2);
                        -lowerRightDiagLineParam(2)
                        -lowerLeftDiagLineParam(2)
                        upperLeftDiagLineParam(2)];
                    
                    temp.P = [eye(2);-eye(2)];
                    temp.q = [0.037;0.037;0.037;0.037];
                    m = [obj.polyPositionSet.P{i,k}(1,:)*linprog(obj.polyPositionSet.P{i,k}(1,:),temp.P,temp.q,[],[],[],[],obj.lProgOptions);
                         obj.polyPositionSet.P{i,k}(2,:)*linprog(obj.polyPositionSet.P{i,k}(2,:),temp.P,temp.q,[],[],[],[],obj.lProgOptions);
                         obj.polyPositionSet.P{i,k}(3,:)*linprog(obj.polyPositionSet.P{i,k}(3,:),temp.P,temp.q,[],[],[],[],obj.lProgOptions);
                         obj.polyPositionSet.P{i,k}(4,:)*linprog(obj.polyPositionSet.P{i,k}(4,:),temp.P,temp.q,[],[],[],[],obj.lProgOptions);
                         obj.polyPositionSet.P{i,k}(5,:)*linprog(obj.polyPositionSet.P{i,k}(5,:),temp.P,temp.q,[],[],[],[],obj.lProgOptions);
                         obj.polyPositionSet.P{i,k}(6,:)*linprog(obj.polyPositionSet.P{i,k}(6,:),temp.P,temp.q,[],[],[],[],obj.lProgOptions);
                         obj.polyPositionSet.P{i,k}(7,:)*linprog(obj.polyPositionSet.P{i,k}(7,:),temp.P,temp.q,[],[],[],[],obj.lProgOptions);
                         obj.polyPositionSet.P{i,k}(8,:)*linprog(obj.polyPositionSet.P{i,k}(8,:),temp.P,temp.q,[],[],[],[],obj.lProgOptions);                                               
                        ];
                    
                    obj.polyPositionSet.q{i,k} = q_nom + m;
                    
                    obj.polyVelocitySet.P{i,k} = [eye(agent(i).nx/2);-eye(agent(i).nx/2)];
                    obj.polyVelocitySet.q{i,k} = [agent(i).x_MAX(2); agent(i).x_MAX(4); -agent(i).x_MIN(2); -agent(i).x_MIN(4)];  
                    
                    
                    
                    for j=1:mission.Nt
                        % nominal target polytope
                        obj.polyTrgSet.P{j,i,k} = [eye(2);-eye(2)];
                        obj.polyTrgSet.q{j,i,k} = [max(mission.Vert_trg{j}(:,1));
                            max(mission.Vert_trg{j}(:,2));
                            -min(mission.Vert_trg{j}(:,1));
                            -min(mission.Vert_trg{j}(:,2))];
                    end
                    
                    for z=1:env.No
                        % nominal obs polytope
                        obj.polyObsSet.P{z,i,k} = [eye(2);-eye(2)];
                        obj.polyObsSet.q{z,i,k} = [max(env.Vert_obs{z}(:,1));
                            max(env.Vert_obs{z}(:,2));
                            -min(env.Vert_obs{z}(:,1));
                            -min(env.Vert_obs{z}(:,2))];
                    end
                end
            end
            
            % Loops 1 to Na-1 and i to Na (with combinations)
            for i=1:Na-1
                for j=i+1:Na
                    for k=1:obj.N+1
                        obj.polyColSet.P{j,i,k} = [eye(2);-eye(2)];
                        obj.polyColSet.q{j,i,k} = [max(agent(i).Vert_robotBody(:,1));
                            max(agent(i).Vert_robotBody(:,2));
                            -min(agent(i).Vert_robotBody(:,1));
                            -min(agent(i).Vert_robotBody(:,2))];
                        
                        
                        % nominal target polytope
                        obj.polyConnSet.P{j,i,k} = [eye(2);-eye(2);-1 1;1 1;1 -1;-1 -1];
                        obj.polyConnSet.q{j,i,k} = [ones(4,1)*agent(i).connRadius; 1.5*ones(4,1)*agent(i).connRadius];
                    end
                end
            end
            
            
            
        end
        % -----------------------------------------------------------------
        function propagateDisturbance(obj,mission,agent)
            obj.L{1} = eye(agent(1).nx);
            
            A = agent(1).Ad_MPC;
            B = agent(1).Bd_MPC;
            
            Q = 1*diag([100 0.1 100 0.1]);
            R = 0.001*diag([1 1]);
            K = dlqr(A,B,Q,R);
            
            for k=1:obj.N
                obj.L{k+1} = (A-B*K)*obj.L{k};
            end
            
        end
        % -----------------------------------------------------------------
        function tightStateSet(obj,agent)
            %m = zeros(2*agent(1).nx,obj.N+1);
            m_pos = zeros(2*agent(1).nx,obj.N+1);
            m_vel = zeros(agent(1).nx,obj.N+1);
            Cp = agent(1).Cd_MPC;
            Cv = [0 1 0 0; 0 0 0 1];
            Na = length(agent);
            
            for i=1:Na
                
                % nominal state polytope
                obj.polyStateSet.P{i,1} = [eye(agent(i).nx);-eye(agent(i).nx)];
                obj.polyStateSet.q{i,1} = [agent(i).x_MAX;-agent(i).x_MIN];
                
                for k=1:obj.N+1
                    
                    % This side of the inequality remains always equal
                    %obj.polyStateSet.P{i,k+1} = [eye(agent(i).nx);-eye(agent(i).nx)];
                    
                    
                    marginPropagation_pos = [
                    obj.polyPositionSet.P{i,k}(1,:)*Cp*obj.L{k}*linprog(obj.polyPositionSet.P{i,k}(1,:)*Cp*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
                    obj.polyPositionSet.P{i,k}(2,:)*Cp*obj.L{k}*linprog(obj.polyPositionSet.P{i,k}(2,:)*Cp*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
                    obj.polyPositionSet.P{i,k}(3,:)*Cp*obj.L{k}*linprog(obj.polyPositionSet.P{i,k}(3,:)*Cp*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
                    obj.polyPositionSet.P{i,k}(4,:)*Cp*obj.L{k}*linprog(obj.polyPositionSet.P{i,k}(4,:)*Cp*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
                    obj.polyPositionSet.P{i,k}(5,:)*Cp*obj.L{k}*linprog(obj.polyPositionSet.P{i,k}(5,:)*Cp*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
                    obj.polyPositionSet.P{i,k}(6,:)*Cp*obj.L{k}*linprog(obj.polyPositionSet.P{i,k}(6,:)*Cp*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
                    obj.polyPositionSet.P{i,k}(7,:)*Cp*obj.L{k}*linprog(obj.polyPositionSet.P{i,k}(7,:)*Cp*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
                    obj.polyPositionSet.P{i,k}(8,:)*Cp*obj.L{k}*linprog(obj.polyPositionSet.P{i,k}(8,:)*Cp*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
                    ];
                    
                    marginPropagation_vel = [
                        obj.polyVelocitySet.P{i,k}(1,:)*Cv*obj.L{k}*linprog(obj.polyVelocitySet.P{i,k}(1,:)*Cv*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
                        obj.polyVelocitySet.P{i,k}(2,:)*Cv*obj.L{k}*linprog(obj.polyVelocitySet.P{i,k}(2,:)*Cv*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
                        obj.polyVelocitySet.P{i,k}(3,:)*Cv*obj.L{k}*linprog(obj.polyVelocitySet.P{i,k}(3,:)*Cv*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
                        obj.polyVelocitySet.P{i,k}(4,:)*Cv*obj.L{k}*linprog(obj.polyVelocitySet.P{i,k}(4,:)*Cv*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
                    ];
                    % This is the way of computing the tightening
                    % proposed in richards and how (2006)
                    % It offers two benefits: first one doesnt need the
                    % MPT toolbox.
%                     marginPropagation = [[1 0 0 0]*obj.L{k}*linprog([1 0 0 0]*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
%                         [0 1 0 0]*obj.L{k}*linprog([0 1 0 0]*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
%                         [0 0 1 0]*obj.L{k}*linprog([0 0 1 0]*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
%                         [0 0 0 1]*obj.L{k}*linprog([0 0 0 1]*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
%                         [-1 0 0 0]*obj.L{k}*linprog([-1 0 0 0]*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
%                         [0 -1 0 0]*obj.L{k}*linprog([0 -1 0 0]*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
%                         [0 0 -1 0]*obj.L{k}*linprog([0 0 -1 0]*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
%                         [0 0 0 -1]*obj.L{k}*linprog([0 0 0 -1]*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions)                                               
%                         ];
                    
%                     m(:,k+1) = m(:,k) - marginPropagation;
%                     obj.polyStateSet.q{i,k+1} = obj.polyStateSet.q{i,1} - m(:,k+1);

                    m_pos(:,k+1) = m_pos(:,k) - marginPropagation_pos;
                    obj.polyPositionSet.q{i,k+1} = obj.polyPositionSet.q{i,1} - m_pos(:,k+1);
                    
                    m_vel(:,k+1) = m_vel(:,k) - marginPropagation_vel;
                    obj.polyVelocitySet.q{i,k+1} = obj.polyVelocitySet.q{i,1} - m_vel(:,k+1);
%                     
                    
                end
                
            end
        end
        % -----------------------------------------------------------------
        function tightTargetSet(obj,mission,agent)
            
            
            m = zeros(4,obj.N+1);
            C = agent(1).Cd_MPC;
            for i=1:mission.Na
                for j=1:mission.Nt
                    % Original set
                   % obj.trgSet{j,i,1} = Polyhedron(mission.Vert_trg{j});
                    
                    
                    % nominal target polytope
                    obj.polyTrgSet.P{j,i,1} = [eye(2);-eye(2)];
                    obj.polyTrgSet.q{j,i,1} = [max(mission.Vert_trg{j}(:,1));
                        max(mission.Vert_trg{j}(:,2));
                        -min(mission.Vert_trg{j}(:,1));
                        -min(mission.Vert_trg{j}(:,2))];
                    
                    for k=1:obj.N+1
                        %obj.trgSet{j,i,k+1} = minHRep(obj.trgSet{j,i,k}-C*obj.L{k}*obj.W{i}); % This is the automatic way of computing the sets using MPT it gives the same results as the method below
                        
                        % This side of the inequality remains always equal
                        obj.polyTrgSet.P{j,i,k+1} = [eye(2);-eye(2)];
                        
                        % This is the way of computing the tightening
                        % proposed in richards and how (2006)
                        % It offers two benefits: first one doesnt need the
                        % MPT toolbox.
                        marginPropagation = [[1 0]*C*obj.L{k}*linprog([1 0]*C*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
                            [0 1]*C*obj.L{k}*linprog([0 1]*C*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
                            [-1 0]*C*obj.L{k}*linprog([-1 0]*C*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
                            [0 -1]*C*obj.L{k}*linprog([0 -1]*C*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions)];
                        
                        m(:,k+1) = m(:,k) - marginPropagation;
                        obj.polyTrgSet.q{j,i,k+1} = obj.polyTrgSet.q{j,i,1} - m(:,k+1);
                        
                        
                    end 
                end
            end
            
            
        end
        % -----------------------------------------------------------------
        function tightObstacleSet(obj,env,agent)
            
            m = zeros(4,obj.N+1);
            C = agent(1).Cd_MPC;
            for i=1:length(agent)
                for j=1:env.No
                    % Original set
                   % obj.obsSet{j,i,1} = Polyhedron(env.Vert_obs{j});
                    
                    obj.polyObsSet.P{j,i,1} = [eye(2);-eye(2)];
                    obj.polyObsSet.q{j,i,1} = [max(env.Vert_obs{j}(:,1));
                        max(env.Vert_obs{j}(:,2));
                        -min(env.Vert_obs{j}(:,1));
                        -min(env.Vert_obs{j}(:,2))];
                    
                    for k=1:obj.N+1
                     %   obj.obsSet{j,i,k+1} = minHRep(obj.obsSet{j,i,k}+C*obj.L{k}*obj.W{i});
                        
                        
                       % This side of the inequality remains always equal
                        obj.polyObsSet.P{j,i,k+1} = [eye(2);-eye(2)];
                        
                        % This is the way of computing the tightening
                        % proposed in richards and how (2006)
                        % It offers two benefits: first one doesnt need the
                        % MPT toolbox.
                        marginPropagation = [[1 0]*C*obj.L{k}*linprog([1 0]*C*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
                            [0 1]*C*obj.L{k}*linprog([0 1]*C*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
                            [-1 0]*C*obj.L{k}*linprog([-1 0]*C*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions);
                            [0 -1]*C*obj.L{k}*linprog([0 -1]*C*obj.L{k},obj.W{i}.A,obj.W{i}.b,[],[],[],[],obj.lProgOptions)];
                        
                        m(:,k+1) = m(:,k) - marginPropagation;
                        obj.polyObsSet.q{j,i,k+1} = obj.polyObsSet.q{j,i,1} + m(:,k+1);
                        
                        
                        
                    end 
                end 
            end
        end
        % -----------------------------------------------------------------
        function tightRobotCollisionSet(obj,agent)
            
            Na = length(agent);
            C = agent(1).Cd_MPC;
            m = zeros(4,obj.N+1);
            
            for i=1:Na-1
                for j=i+1:Na
                    % Original set
                  %  obj.colSet{j,i,1} = Polyhedron(agent(i).Vert_robotBody);
                  
                    % gotta sum disturbances of each combination of robots
                    % for worst case scenario
                    Wij = minHRep(obj.W{i}+obj.W{j});
                    
                    % nominal target polytope
                    obj.polyColSet.P{j,i,1} = [eye(2);-eye(2)];
                    obj.polyColSet.q{j,i,1} = [max(agent(i).Vert_robotBody(:,1));
                        max(agent(i).Vert_robotBody(:,2));
                        -min(agent(i).Vert_robotBody(:,1));
                        -min(agent(i).Vert_robotBody(:,2))];
                    
                    for k=1:obj.N+1
                      %  obj.colSet{j,i,k+1} = minHRep(obj.colSet{j,i,k}+C*obj.L{k}*Wij);
                        
                        % This side of the inequality remains always equal
                        obj.polyColSet.P{j,i,k+1} = [eye(2);-eye(2)];
                        
                        % This is the way of computing the tightening
                        % proposed in richards and how (2006)
                        % It offers two benefits: first one doesnt need the
                        % MPT toolbox.
                        marginPropagation = [[1 0]*C*obj.L{k}*linprog([1 0]*C*obj.L{k},Wij.A,Wij.b,[],[],[],[],obj.lProgOptions);
                            [0 1]*C*obj.L{k}*linprog([0 1]*C*obj.L{k},Wij.A,Wij.b,[],[],[],[],obj.lProgOptions);
                            [-1 0]*C*obj.L{k}*linprog([-1 0]*C*obj.L{k},Wij.A,Wij.b,[],[],[],[],obj.lProgOptions);
                            [0 -1]*C*obj.L{k}*linprog([0 -1]*C*obj.L{k},Wij.A,Wij.b,[],[],[],[],obj.lProgOptions)];
                        
                        m(:,k+1) = m(:,k) - marginPropagation;
                        obj.polyColSet.q{j,i,k+1} = obj.polyColSet.q{j,i,1} + m(:,k+1);
                        
                    end
                end
            end 
            
        end
        % -----------------------------------------------------------------
        function tightConnectivitySet(obj,agent)
            
            Na = length(agent);
            C = agent(1).Cd_MPC;
            m = zeros(8,obj.N+1);
            
            for i=1:Na-1
                for j=i+1:Na
                    % Original set
                  %  obj.colSet{j,i,1} = Polyhedron(agent(i).Vert_robotBody);
                  
                    % gotta sum disturbances of each combination of robots
                    % for worst case scenario
                    Wij = minHRep(obj.W{i}+obj.W{j});
                    
                    % nominal target polytope
                    obj.polyConnSet.P{j,i,1} = [eye(2);-eye(2);-1 1;1 1;1 -1;-1 -1];
                    obj.polyConnSet.q{j,i,1} = [ones(4,1)*agent(i).connRadius; 1.5*ones(4,1)*agent(i).connRadius];

                    
                    for k=1:obj.N+1
                        
                        % This side of the inequality remains always equal
                        obj.polyConnSet.P{j,i,k+1} = [eye(2);-eye(2);-1 1;1 1;1 -1;-1 -1];
                        
                        % This is the way of computing the tightening
                        % proposed in richards and how (2006)
                        % It offers two benefits: first one doesnt need the
                        % MPT toolbox.
                        P = obj.polyConnSet.P{j,i,k+1};
                        marginPropagation = [P(1,:)*C*obj.L{k}*linprog(P(1,:)*C*obj.L{k},Wij.A,Wij.b,[],[],[],[],obj.lProgOptions);
                            P(2,:)*C*obj.L{k}*linprog(P(2,:)*C*obj.L{k},Wij.A,Wij.b,[],[],[],[],obj.lProgOptions);
                            P(3,:)*C*obj.L{k}*linprog(P(3,:)*C*obj.L{k},Wij.A,Wij.b,[],[],[],[],obj.lProgOptions);
                            P(4,:)*C*obj.L{k}*linprog(P(4,:)*C*obj.L{k},Wij.A,Wij.b,[],[],[],[],obj.lProgOptions);
                            P(5,:)*C*obj.L{k}*linprog(P(5,:)*C*obj.L{k},Wij.A,Wij.b,[],[],[],[],obj.lProgOptions);
                            P(6,:)*C*obj.L{k}*linprog(P(6,:)*C*obj.L{k},Wij.A,Wij.b,[],[],[],[],obj.lProgOptions);
                            P(7,:)*C*obj.L{k}*linprog(P(7,:)*C*obj.L{k},Wij.A,Wij.b,[],[],[],[],obj.lProgOptions);
                            P(8,:)*C*obj.L{k}*linprog(P(8,:)*C*obj.L{k},Wij.A,Wij.b,[],[],[],[],obj.lProgOptions)
                            ];
                        
                        m(:,k+1) = m(:,k) - marginPropagation;
                        obj.polyConnSet.q{j,i,k+1} = obj.polyConnSet.q{j,i,1} - m(:,k+1);
                        
                    end
                end
            end 
            
        end
        % -----------------------------------------------------------------
        function inspectTightenedSets(obj,mission,env)
            
            figure(); hold on;
            for j=1:mission.Nt
                for k=1:obj.N+1
                  polyObj = Polyhedron(obj.polyTrgSet.P{j,1,1},obj.polyTrgSet.q{j,1,k});
                  polyObj.plot('alpha',k*0.05);
                end
            end
            xlim([-0.75 0.75]); ylim([-0.65 0.65]);
            
            for j=1:env.No
                for k=obj.N+1:-1:1
                    if k==1
                        color = 'black';
                        alpha = 1;
                    else
                        color = 'blue';
                        alpha = 0.01*k;
                    end
                     polyObj = Polyhedron(obj.polyObsSet.P{j,1,1},obj.polyObsSet.q{j,1,k});
                     polyObj.plot('color',color,'alpha',alpha);
                end
            end
            
         %    for i=1:obj.N;plot(Polyhedron(obj.polyColSet.P{2,1,i},obj.polyColSet.q{2,1,i}),'color','green','alpha',i*0.1); hold on; end
            
        end
        % -----------------------------------------------------------------
        
        %%
    end
end

