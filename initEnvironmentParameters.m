function env = initEnvironmentParameters(environmentType)
% This function loads some environmental data and creates a environment
% data structure.

switch environmentType
    
    
    case 'Scenario_1'
        % Operation region
        env.opRegionWidth = 1.3;
        env.opRegionLength = 1.5;
        env.opRegionCenter = [0,0];
        env.fieldLength = 1.5;
        env.fieldWidth = 1.3;
        env.fieldDiagLength = 0.07;
        env.fieldDiagWidth = 0.07;
        
        
        % Obstacle's Centers and lengths
        OBS_CENTER(1,:) = [0.2,0];
        OBS_LENGTH(1,:) = [0.3,0.08];
        
        OBS_CENTER(2,:) = [-0.20,0.475];
        OBS_LENGTH(2,:) = [0.08,0.35+0.005];
        
        OBS_CENTER(3,:) = [-0.20,-0.475+0.0035];
        OBS_LENGTH(3,:) = [0.08,0.35+0.007];
        
        env.No = size(OBS_CENTER,1);
        
        % Polytope and vertices
        for n=1:env.No
            
            % Vertices
            env.Vert_obs{n}(1,:) = [OBS_CENTER(n,1) + OBS_LENGTH(n,1)/2,...
                OBS_CENTER(n,2) + OBS_LENGTH(n,2)/2];
            
            env.Vert_obs{n}(2,:) = [OBS_CENTER(n,1) - OBS_LENGTH(n,1)/2,...
                OBS_CENTER(n,2) + OBS_LENGTH(n,2)/2];
            
            env.Vert_obs{n}(3,:) = [OBS_CENTER(n,1) - OBS_LENGTH(n,1)/2,...
                OBS_CENTER(n,2) - OBS_LENGTH(n,2)/2];
            
            env.Vert_obs{n}(4,:) = [OBS_CENTER(n,1) + OBS_LENGTH(n,1)/2,...
                OBS_CENTER(n,2) - OBS_LENGTH(n,2)/2];
            
            env.polyOrigObs{n} = Polyhedron([
                OBS_CENTER(n,1)+(OBS_LENGTH(n,1))/2-0.037,OBS_CENTER(n,2)+(OBS_LENGTH(n,2))/2-0.037;
                OBS_CENTER(n,1)-(OBS_LENGTH(n,1))/2+0.037,OBS_CENTER(n,2)+(OBS_LENGTH(n,2))/2-0.037;
                OBS_CENTER(n,1)-(OBS_LENGTH(n,1))/2+0.037,OBS_CENTER(n,2)-(OBS_LENGTH(n,2))/2+0.037;
                OBS_CENTER(n,1)+(OBS_LENGTH(n,1))/2-0.037,OBS_CENTER(n,2)-(OBS_LENGTH(n,2))/2+0.037]);
            
        end
        
        % Number of polytope sides
        env.Nos = length(env.Vert_obs{1});
        
        
% -------------------------------------------------------------------------
    case 'Scenario_2'
        % Operation region
        env.opRegionWidth = 1.3;
        env.opRegionLength = 1.5;
        env.opRegionCenter = [0,0];
        env.fieldLength = 1.5;
        env.fieldWidth = 1.3;
        env.fieldDiagLength = 0.07;
        env.fieldDiagWidth = 0.07;
        
        
        % Obstacle's Centers and lengths
        OBS_CENTER(1,:) = [-0.3,0.4];
        OBS_LENGTH(1,:) = [0.03,0.5+0.01];
        OBS_CENTER(2,:) = [-0.3,-0.4];
        OBS_LENGTH(2,:) = [0.03,0.5+0.01];
        OBS_CENTER(3,:) = [-0.135,-0.165];
        OBS_LENGTH(3,:) = [0.300+0.01,0.03+0.01];
        OBS_CENTER(4,:) = [0.200,0.45];
        OBS_LENGTH(4,:) = [0.03,0.4+0.01];
        OBS_CENTER(5,:) = [0.400,-0.350];
        OBS_LENGTH(5,:) = [0.03,0.600+0.01];
        
        env.No = size(OBS_CENTER,1);
        
        % Polytope and vertices
        for n=1:env.No
            
            % Vertices
            env.Vert_obs{n}(1,:) = [OBS_CENTER(n,1) + OBS_LENGTH(n,1)/2,...
                OBS_CENTER(n,2) + OBS_LENGTH(n,2)/2];
            
            env.Vert_obs{n}(2,:) = [OBS_CENTER(n,1) - OBS_LENGTH(n,1)/2,...
                OBS_CENTER(n,2) + OBS_LENGTH(n,2)/2];
            
            env.Vert_obs{n}(3,:) = [OBS_CENTER(n,1) - OBS_LENGTH(n,1)/2,...
                OBS_CENTER(n,2) - OBS_LENGTH(n,2)/2];
            
            env.Vert_obs{n}(4,:) = [OBS_CENTER(n,1) + OBS_LENGTH(n,1)/2,...
                OBS_CENTER(n,2) - OBS_LENGTH(n,2)/2];
            
            env.polyOrigObs{n} = Polyhedron([
                OBS_CENTER(n,1)+(OBS_LENGTH(n,1))/2,OBS_CENTER(n,2)+(OBS_LENGTH(n,2))/2;
                OBS_CENTER(n,1)-(OBS_LENGTH(n,1))/2,OBS_CENTER(n,2)+(OBS_LENGTH(n,2))/2;
                OBS_CENTER(n,1)-(OBS_LENGTH(n,1))/2,OBS_CENTER(n,2)-(OBS_LENGTH(n,2))/2;
                OBS_CENTER(n,1)+(OBS_LENGTH(n,1))/2,OBS_CENTER(n,2)-(OBS_LENGTH(n,2))/2]);
            
            
        end
        
        
        % Number of polytope sides
        env.Nos = length(env.Vert_obs{1});
        
end

