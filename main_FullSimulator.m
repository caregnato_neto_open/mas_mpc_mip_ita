%********************************************************************
% Instituto Tecnologico de Aeronautica                              %
% 28 Fev 2021                                                       %
% Author: Angelo Caregnato-Neto, Marcos Maximo, Rubens J M Afonso   %
%********************************************************************

% This code implement the optimal trajectory generation for a MAS using MPC
% with a MIP encoding. The system considered is a differential robot.

%% Preliminaries
clearvars; close all; clc; yalmip("clear");
tic; disp("-> Building scenario...");

ENVIRONMENT = "Scenario_1";
MISSION = "Scenario_1";
ROBOT_TYPE = "VSS";
SOLVER = 'gurobi';

% Initializations
env = initEnvironmentParameters(ENVIRONMENT);
mission = initMissionParameters(MISSION);
MAS = initMASParameters(mission,env,ROBOT_TYPE);
solverOptions = initSolverParameters(SOLVER);
for j=1:mission.Na; agent(j) = AgentsClass; agent(j).InitializeVariables(MAS,j,mission);end
MPC = MPCClass; 
MPC.InitializeVariables(env,agent,mission,solverOptions);

% Load disturbance set
load('W');

CT = ConstraintTighteningClass(W,MPC,mission,agent,env);
prompt = "Use constraint tightening? (Y/N)";
x = input(prompt,"s");
if lower(x)=="y"
CT.propagateDisturbance(mission,agent);
CT.tightStateSet(agent);
CT.tightTargetSet(mission,agent);
CT.tightObstacleSet(env,agent);
CT.tightRobotCollisionSet(agent);
CT.tightConnectivitySet(agent)
CT.inspectTightenedSets(mission,env);
end

% Data processor and displayer initialization
DataProcessor = DataProcessorClass;
DataProcessor.InitializeVariables(mission,MPC,[]);
Displayer = DisplayerClass;
Displayer.GeneratePlotColors(mission);
Displayer.InitializeDisplayerParameters(env);
Displayer.PrintScenarioInfo(mission,MAS)

% Auxiliary
kIndex = 0;

% Initialization block timer
iniTime = toc; disp("-> Done! " + "(" + iniTime + "s)"); disp("->");
%% Planner/Recov mode constraint and cost construction
tic; disp("-> Building optimization problem...")

%Builds constraints and cost of planner mode
MPC.BuildPlannerConstraints(agent,env,mission,CT);
MPC.BuildPlannerObjective(mission);

% Problem construction block timer
constTime = toc; disp("-> Done! " + "(" + constTime + "s)"); disp("->")

%% Pre-optimization (optimizer command)
tic; disp("-> Pre-optimizing...");

% Creates planner and recovery mode objects
MPC.preOptimize();
%MPC.Planner.options.verbose=1;

% Pre-optimization block timer
preOptTime = toc; disp("-> Done! " + "(" + preOptTime + "s)"); disp("->");
%% Delayed input formulation initialization
accPast = -0.0001*[0.1 0.1 0.1 0.1 0.1;0.1 0.1 0.1 0.1 0.1]; % start from "almost" rest to avoid singularity
xNext_MPC = [agent(1).x(:,1),agent(2).x(:,1),agent(3).x(:,1),agent(4).x(:,1),agent(5).x(:,1)];


%% Simulation
%for i=1:MAS.kfinal_plannerLoop
for i=1:MPC.N
    tic; disp("-> Optimizing - Planner loop iteration: " + i);
    
% ------- Trajectory planning loop (1Hz) ----------------------------------

    % Builds full state vector with the states of all agents
    xAll_k = MPC.BuildFullStateVector(agent,kIndex+1);
    
    x_feedback = [] ;
    for p=1:mission.Na
    x_feedback = [x_feedback;xAll_k(:,p);xNext_MPC(:,p)]; 
    end
   
    % ONLY RUN IF WITH WARMSTART
    if i>1
    decisions = MPC.GetDecisions(sol_i,mission);
    for in=1:size(decisions,1)
       assign(MPC.B_target(MPC.B_targetIndex(decisions(in,1),decisions(in,2),decisions(in,3))),1);
    end
    end
    
x_nom = zeros(2,5);


    % Optimizes using current state
    [sol_i,isThereSolution] = MPC.Optimize(x_feedback,MPC.visitedTargets);
    if ~isThereSolution; break;  end % stops loop if there is no solution
    
    % Extract trajectories from solution (waypoints and velocities)
    [waypoints,velocities,accelerations,Nopt] = MPC.ExtractTrajectories(sol_i,mission);
    MPC.trajectories(i).waypoints = waypoints;
    MPC.trajectories(i).velocities = velocities;
    MPC.trajectories(i).accelerations = accelerations;
    MPC.trajectories(i).optimalHorizon = Nopt-1;
    
%------------- Position control loop (60Hz due to camera limits)) ---------
    for j=1:MAS.kfinal_posCtrlLoop
        % Index considering iteration of external loop
        jIndex = (i-1)*MAS.kfinal_posCtrlLoop + j;
        
        % This index is used to access the values of the internal loop variables
        jth_kIndex = kIndex+1;
        
        for n=1:mission.Na % agent loop
            
            % Trajectory treatment
            [agent(n).posCtrl.posCommand(:,:,i),agent(n).posCtrl.velCommand(:,:,i),...
                agent(n).posCtrl.accCommand(:,:,i)] = MPC.BuildTrajectoriesCommands(MPC.trajectories(i),agent(n),n,accPast);
            
            % Trajectory tracking feedback linearization compensator
            agent(n).linVelCommand(1) = norm(agent(n).posCtrl.velCommand(:,2,1));
            
            [agent(n).linVelCommand(jIndex+1),agent(n).angVelCommand(jIndex)] = ...
                agent(n).TrackingController(agent(n).xUnicycle(:,jth_kIndex),...
                agent(n).vUnicycle(1:2,jth_kIndex),... %1:2 only linear vel
                agent(n).linVelCommand(jIndex),...
                agent(n).posCtrl.posCommand(:,j+1,i),...
                agent(n).posCtrl.velCommand(:,j+1,i), ...
                agent(n).posCtrl.accCommand(:,j,i),jIndex);
            
            % Compute wheel velocities
            agent(n).posCtrl.wheelVelCommand(:,jIndex) = ...
                agent(n).ComputeWheelVelCommand(agent(n).linVelCommand(jIndex),...
                agent(n).angVelCommand(jIndex));
            
%---------------- Motor loop (240Hz) --------------------------------------
            for k=1:MAS.kfinal_motorLoop
                
                % Index consering iteration of external loops
                kIndex = (i-1)*MAS.kfinal_motorLoop*MAS.kfinal_posCtrlLoop + (j-1)*MAS.kfinal_motorLoop + k;
                
                % Wheel motor controller
                agent(n).motor.voltages(:,kIndex) =...
                    agent(n).MotorController(agent(n).motor.quantizedAxisVel(:,kIndex),...
                    agent(n).posCtrl.wheelVelCommand(:,jIndex)*agent(n).motor.N);
                
                % Integration of FULL model (motor+gearBox+robotBody+kinematics)
                % using ODE45: sol = ode45(@unicycleModel(wheelVel),integInterval,x0)
                sol = ode45(@(t,xUni_k) agent(n).FullRobotDynamics(t,xUni_k,agent(n).motor.voltages(:,kIndex)),...
                    [0,agent(n).Ts_motorLoop],[agent(n).xUnicycle(:,kIndex);agent(n).motor.wheelVel(:,kIndex)]);
                
                % Extracts integration solution
                agent(n).motor.wheelVel(:,kIndex+1) =  sol.y(4:end,end);
                agent(n).xUnicycle(:,kIndex+1) = sol.y(1:3,end);
                
                % Determines linear velocities
                agent(n).vUnicycle(:,kIndex+1) = ...
                    (agent(n).xUnicycle(:,kIndex+1) - agent(n).xUnicycle(:,kIndex))/agent(n).Ts_motorLoop;
                
                % Builds MPC state vector x = (r_x,v_x,r_y,v_y)
                agent(n).x(:,kIndex+1) = [agent(n).xUnicycle(1,kIndex+1);
                    agent(n).vUnicycle(1,kIndex+1);
                    agent(n).xUnicycle(2,kIndex+1);
                    agent(n).vUnicycle(2,kIndex+1)];
                
                agent(n).y(:,kIndex+1) = agent(n).xUnicycle(1:2,kIndex+1);
                
                % SENSOR SIMULATION -----------------------------------------------
                % Encoder dynamics (the sensor counts motor axis revolutions not wheel)
                [nPulses,agent(n).motor.angles(:,kIndex+1)] =...
                    agent(n).encoderDynamics(agent(n).motor.wheelVel,agent(n).motor.angles(:,kIndex),kIndex);
                
                % Quantization
                [agent(n).motor.quantizedAxisAngles(:,kIndex+1), agent(n).motor.quantizedAxisVel(:,kIndex+1)] =...
                    agent(n).encoderQuantization(nPulses,agent(n).motor.quantizedAxisAngles(:,kIndex));
                
                % Converts quantized axis vel. into quantized wheel vel.
                agent(n).motor.quantizedWheelVel(:,kIndex+1) =...
                    agent(n).motor.quantizedAxisVel(:,kIndex+1)/agent(n).motor.N;

                % Achieved linear velocity
                [agent(n).linVel(kIndex+1),agent(n).angVel(kIndex+1)] = ...
                    agent(n).ComputeRealUnicycleVel(agent(n).motor.quantizedWheelVel(:,kIndex+1));
                
            end % end motor control loop  
            
        end % end agent iteration loop
        
        % TERMINATION CONDITIONS
        isMissionOver =  MPC.CheckTargetVisitation(agent,mission,kIndex,CT); % Verifies target visitation
        %if isMissionOver; break; end % Ends loop if the last one was visited
        
    end % end position control loop
    
    % saves a(k+1|k) and computes x(k+2|k) = A*x(k+1|k) + B*a(k+1|k)
    for z=1:mission.Na
        accPast(:,z) = sol_i{2}(MPC.uIndex(:,1,z));
        xNext_MPC(:,z) = agent(z).Ad_MPC*agent(z).x(:,kIndex+1) + agent(z).Bd_MPC*accPast(:,z);
    end
   
    % End of decentralized stuff ------------------------------------------
    
    % Plot trajectory calculated by the MPC at instant k
    Displayer.PlotVirtualTrajectories(MPC,i,env,mission,MAS); hold on;
    % Loop iteration timer
    iterationTime = toc; disp("-> " +i+"th iteration Done! " + "(" + iterationTime + "s)"); disp("->");
end % end path planning loop

%% Data Processment
tic; disp("-> Processing data...");

% Determines Terminal step "i", "j", "k"
DataProcessor.terminalStep_i = i;
DataProcessor.terminalStep_k = kIndex+1;
optimalHorizonts = [];
nBinaryVar=[]; nContinuousVar=[];
graphsPhys =[];
nCutVertices = [];
avgCutVertices = [];

% Number of targets visited during mission
nVisitedTargets = nnz(DataProcessor.wasTargetVisited);

% Communication graph associated with real trajectories
%graphsPhys = DataProcessor.BuildConnectivityGraphs(agent,mission,env,0,MPC);

% Cut vertices from communication graph during mission
%[nCutVertices,avgCutVertices] = DataProcessor.FindNumberCutVertex(graphsPhys,[],mission,0);


% Calculates the cost using the real mission parameters
missionObjectiveValue = DataProcessor.FindMissionObjective(agent,nVisitedTargets,MPC);

% Compact data for plots, report, and saving into a data structure
missionData = DataProcessor.CompactMissionData(agent,MAS,env,optimalHorizonts,MPC.N,...
    mission, solverOptions, missionObjectiveValue,nVisitedTargets,...
    nBinaryVar,nContinuousVar,nCutVertices,avgCutVertices,[],[],graphsPhys,[],...
    MPC.nConst,MPC.CONNECTIVITY_CONSTRAINT_TYPE,[],DataProcessor.kFail,...
    [],i,jIndex,kIndex);


% Data processment timer
processTime = toc; disp("-> Done! " + "(" + processTime + "s)"); disp("->");
%% Display results
tic; disp("-> Displaying results..."); %close all;

%Preliminaries
Displayer.InitializeData(missionData);

%Plots
% Displayer.PlotAgentInputs();
% Displayer.PlotAgentVelocities();
% Displayer.PlotOptimalHorizonts();
% Displayer.PlotCutVertices();
% Displayer.PlotGraphs();
% Displayer.PlotVirtualTrajectories(MPC,1,env,mission,MAS)
% Displayer.PlotWheelVelocities([]);
% Displayer.PlotUnicycleVelocities();
% Displayer.PlotUnicycleOrientation();
% Displayer.PlotMotorVoltages([]);
Displayer.PlotRealTrajectory(CT); hold on;
collisionTable = checkSimulationCollisions(agent,MPC);
Displayer.PlotCollisions(collisionTable,agent);

%Displayer timer
displayTime = toc; disp("-> Done! " + "(" + displayTime + "s)"); disp("->");

