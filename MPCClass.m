classdef MPCClass < handle
    % This class represents the model predictive controller. The methods
    % are used to build the opt.constraints and cost of the optimization
    % problem. The parameters of the controller must be defined in the
    % InitializeParameters method.
    
    properties
        N = [];
        gamma = [];
        targetReward = [];
        epsilon_obs = [];
        epsilon_col = [];
        X = [];
        U = [];
        abs_U = [];
        B_hor = [];
        B_target = [];
        B_obs = [];
        B_col = [];
        B_con = [];
        x0 = [];
        BigM = [];
        xIndex = [];
        uIndex = [];
        B_targetIndex = [];
        B_obsIndex = [];
        B_colIndex = [];
        B_conIndex = [];
        B_colDecIndex = [];
        B_losIndex = [];
        B_obsSampleLOSIndex = [];
        nConst = [];
        CONNECTIVITY_CONSTRAINT_TYPE = [];
        Planner = [];
        solverOptions = [];
        iniCondConst  = [];
        dynConst_X = [];
        dynConst_Y = [];
        AbsUConst = [];
        stateConst = [];
        inputConst = [];
        outputConst = [];
        B_horConst = [];
        TerminalSetConst = [];
        B_targetConst = [];
        TargetHorEqualityConst = [];
        Visitb4HorizontConst = [];
        ObstAvoidConst = [];
        BinObstAvoidConst = [];
        AvoidCuttingObsConst = [];
        collisionAvoidConst = [];
        BinColAvoidConst = [];
        BinColAvoidInitConst = [];
        conRegionConst = [];
        edgesConst = [];
        noCycleConst = [];
        degreeSumConstraint = [];
        minDegreeConstraint = [];
        plannerConstraints = [];
        plannerObjective = [];
        conRegionConstRecov = [];
        AvoidInterSampleCollConst =[];
        visitedTargets = []

        %
        fixedHorConstraint = [];
        Nfixed = [];
        trajectories = [];
        wasTargetVisited = [];
        
        % Sets for constraint tightening
        stateSet = [];
        inputSet = [];
        obsSet = [];
        colSet = [];
        trgSet = [];
        connSet = [];
        
        %delayed input formulation
        nextState = [];
        delayedDynConst_X = [];
        delayedInputFlag = [];
        x_nom = [];
        %test
        x_fb = [];
        
    end
    
    methods
        %------------------------------------------------------------------
        function InitializeVariables(opt,env,agent,mission,solverOps)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method initializes all general variables which will be
            % used in the remaining methods.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                     
            % MPC Parameters
            opt.N = 6; % constraint tightening experiment
            opt.gamma = 0.2; % control weight
            opt.targetReward = 3*ones(1,(opt.N+1)*mission.Na*mission.Nt);
            opt.BigM = 100;
            opt.CONNECTIVITY_CONSTRAINT_TYPE = 'ROBUST_SUMVERTICEDEGREE'; % 'COMPLETE' 'ROBUST_SUMVERTICEDEGREE', 'ROBUST_MINVERTICEDEGREE', 'HEURISTIC'
            opt.visitedTargets = zeros(mission.Nt,1);
            opt.delayedInputFlag = 1;
%             
%             % Optimization variables
            opt.X = sdpvar((opt.N+1)*mission.Na*agent(1).nx,1); % N+1 due to x0
            opt.U = sdpvar(opt.N*mission.Na*agent(1).nu,1);
            opt.abs_U = sdpvar(opt.N*mission.Na*agent(1).nu,1);
            opt.B_hor = binvar(opt.N+1,1);
            opt.B_target = binvar((opt.N+1)*mission.Na*mission.Nt,1);
            opt.B_obs = binvar((opt.N+1)*mission.Na*env.Nos*env.No,1);
            opt.B_col = binvar( (opt.N+1)*mission.Na*(mission.Na-1)/2*agent(1).Ncs,1);
            opt.B_con = binvar((opt.N+1)*sum(1:mission.Na-1),1);
         
            
            % Feedback variables. Optimizer requires them to be sdpvar/binvar.
            opt.x_fb = sdpvar(agent(1).nx*2*mission.Na,1);
           opt.wasTargetVisited = binvar(mission.Nt,1);
            opt.x_nom = sdpvar((agent(1).nx/2)*mission.Na,1);

            % Initialize solver options
            opt.solverOptions = solverOps;
            opt.solverOptions.outputs = {opt.X, opt.U,opt.B_hor, opt.B_obs, opt.B_target,opt.x_fb};%,opt.B_col};
            
            % Create indices vector. This is an effort to clear the code.
            % Consider the optimization vector X. The vector xIndex is used
            % to easily handle the desired indices of X. For example,
            % xIndex(:,i,j) is the state vector of the jth agent at k+i.
            for j=1:mission.Na
                for k=1:opt.N+1
                    
                    % Index logic of X
                    opt.xIndex(:,k,j)=(j-1)*((opt.N+1)*agent(j).nx)+(agent(j).nx*(k-1)+1):...
                        (j-1)*((opt.N+1)*agent(j).nx)+(agent(j).nx*k);
                    
                    % Index logic of B_obs
                    for n=1:env.No
                        opt.B_obsIndex(:,n,k,j) = ...
                            (j-1)*(opt.N+1)*env.Nos*env.No + env.Nos*((k-1)*env.No+n-1) + 1:...
                            (j-1)*(opt.N+1)*env.Nos*env.No + env.Nos*((k-1)*env.No+n-1)+env.Nos;
                    end
                    
                    % Index logic of B_target
                    for t=1:mission.Nt
                        opt.B_targetIndex(t,k,j) = (j-1)*(opt.N+1)*mission.Nt+(t-1)*(opt.N+1) + k;
                    end
                    
                    % Index logic of B_con
                    nCon = mission.Na-1:-1:1;
                    for i=j+1:mission.Na
                        opt.B_conIndex(i,k,j) = sum(nCon(1:j-1))*(opt.N+1) + (k-1)*(mission.Na-j)+i-j;
                    end
                    
                end
                
                % Index logic of U
                for k=1:opt.N
                    opt.uIndex(:,k,j)=...
                        (j-1)*(opt.N*agent(j).nu)+(agent(j).nu*(k-1)+1):...
                        (j-1)*(opt.N*agent(j).nu)+(agent(j).nu*k);
                end
            end % end index logic loops
            
            % Index logic of B_col
            for j=1:mission.Na-1
                lambda = sum(mission.Na-1:-1:mission.Na-(j-1))*(opt.N+1)*agent(j).Ncs;
                for i=j+1:mission.Na
                    beta = (i-(j+1))*(opt.N+1)*agent(j).Ncs;
                    for k=1:opt.N+1
                        alfa_e = (k-1)*agent(j).Ncs+1;
                        alfa_d = k*agent(j).Ncs;
                        
                        opt.B_colIndex(:,i,k,j) = (lambda+beta+alfa_e):(lambda+beta+alfa_d);
                    end
                end
            end

        end
        
        %------------------------------------------------------------------
        function BuildInitialCondConst(opt,mission)
            for j=1:mission.Na
                ind = (j-1)*2*4+1:j*2*4;
                jthAgent_state = opt.x_fb(ind);
                
                opt.iniCondConst = [opt.iniCondConst; opt.X(opt.xIndex(:,1,j)) == jthAgent_state(1:4)]; 
                opt.iniCondConst = [opt.iniCondConst; opt.X(opt.xIndex(:,2,j)) == jthAgent_state(4+1:end)];             
            end
        end
        %------------------------------------------------------------------
        function BuildDelayedDynamicConst(opt,agent,mission)

            
            for j=1:mission.Na
                for k=2:opt.N 
                    delayedDynConst_X_k = [opt.X(opt.xIndex(:,k+1,j)) <= agent(j).Ad_MPC*opt.X(opt.xIndex(:,k,j))+agent(j).Bd_MPC*opt.U(opt.uIndex(:,k-1,j))+ones(agent(j).nx,1)*opt.BigM*sum(opt.B_hor(1:k)),...
                                          -opt.X(opt.xIndex(:,k+1,j)) <=-agent(j).Ad_MPC*opt.X(opt.xIndex(:,k,j))-agent(j).Bd_MPC*opt.U(opt.uIndex(:,k-1,j))+ones(agent(j).nx,1)*opt.BigM*sum(opt.B_hor(1:k))];
                    
                    opt.delayedDynConst_X = [opt.delayedDynConst_X, delayedDynConst_X_k];
                end 
            end
            % Calculate constraint size (for report purposes)
            opt.nConst.dyn = agent(1).nx*length(opt.dynConst_X);
            opt.nConst.iniCond = (agent(1).nx+agent(1).ny)*length(opt.iniCondConst)/2;
            
        end
        %------------------------------------------------------------------
        function BuildStateControlConstraints(opt,agent,mission,CT)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method builds the state/control opt.constraints for the
            % predictive controller. The outputs are max and min
            % constraint objects for the state and control.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Builds opt.constraints
            Cp = agent(1).Cd_MPC;
            Cv = [0 1 0 0;0 0 0 1];
            for j=1:mission.Na
                for k=3:opt.N+1
                      % pos
                      opt.stateConst = [opt.stateConst,...
                          CT.polyPositionSet.P{j,k}*Cp*opt.X(opt.xIndex(:,k,j)) <= CT.polyPositionSet.q{j,k}+ones(2*agent(j).nx,1)*opt.BigM*sum(opt.B_hor(1:k-1))];

                      % vel
                      opt.stateConst = [opt.stateConst,...
                          CT.polyVelocitySet.P{j,k}*Cv*opt.X(opt.xIndex(:,k,j)) <= CT.polyVelocitySet.q{j,k}+ones(agent(j).nx,1)*opt.BigM*sum(opt.B_hor(1:k-1))];
                end
            end
            
             for j=1:mission.Na
                for k=1:opt.N
                    % (11f) and (11g)
                    opt.inputConst = [opt.inputConst,...
                        CT.polyInputSet(1).P(:,:,1)*opt.U(opt.uIndex(:,k,j)) <= CT.polyInputSet(1).q(:,1)+ones(2*agent(j).nu,1)*opt.BigM*sum(opt.B_hor(1:k-1))];
                end
            end
            
            % Calculate constraint size (for report purposes)
            opt.nConst.state = agent(1).nx*length(opt.stateConst);
            opt.nConst.control = agent(1).nu*length(opt.inputConst);
            opt.nConst.output = agent(1).ny*length(opt.outputConst);
        end
        %------------------------------------------------------------------
        function BuildObstacleAvoidanceConstraints(opt,env,mission,agent,CT)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method builds the obstacle avoidance opt.constraints. The
            % following opt.constraints are here implemented: (11h), (11i),
            % and (11q).
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % Builds opt.constraints
            for j=1:mission.Na
               
                for k=3:opt.N+1
                    for n=1:env.No

           
                        pos_jthAgent = [opt.X(opt.xIndex(1,k,j));opt.X(opt.xIndex(3,k,j))];

                       opt.ObstAvoidConst = ...
                             [opt.ObstAvoidConst,-CT.polyObsSet.P{n,j,k}*pos_jthAgent <= -CT.polyObsSet.q{n,j,k}+opt.BigM*(ones(env.Nos,1)-opt.B_obs(opt.B_obsIndex(:,n,k,j)))+opt.BigM*ones(env.Nos,1)*sum(opt.B_hor(1:k-1))];

                            previousPos_jthAgent = [opt.X(opt.xIndex(1,k-1,j));opt.X(opt.xIndex(3,k-1,j))];

                         
                         opt.AvoidCuttingObsConst = ...
                            [opt.AvoidCuttingObsConst,-CT.polyObsSet.P{n,j,k-1}*previousPos_jthAgent<=-CT.polyObsSet.q{n,j,k-1}+opt.BigM*(ones(env.Nos,1)-opt.B_obs(opt.B_obsIndex(:,n,k,j)))+opt.BigM*ones(env.Nos,1)*sum(opt.B_hor(1:k-1))];
                      
                        % Constraints on the obstacle avoidance binaries (11q)
                        opt.BinObstAvoidConst = [opt.BinObstAvoidConst; sum(opt.B_obs(opt.B_obsIndex(:,n,k,j))) >= 1];
                        
                    end
                end
                
            end
            
            % Calculate constraint size (for report purposes)
            opt.nConst.obstAvoid = env.Nos*length(opt.ObstAvoidConst) + length(opt.BinObstAvoidConst)...
                + env.Nos*length(opt.AvoidCuttingObsConst);
        end
        %------------------------------------------------------------------
        function BuildCollisionAvoidanceConstraints(opt,mission,agent,CT)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method builds the obstacle collision opt.constraints. The
            % following opt.constraints are here implemented: (11j), (11r), and (11s).
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            
            for j=1:mission.Na-1
                for k=3:opt.N+1
                    for i=j+1:mission.Na
                        % (11j)
                        coordDifference = [opt.X(opt.xIndex(1,k,j));opt.X(opt.xIndex(3,k,j))] - [opt.X(opt.xIndex(1,k,i));opt.X(opt.xIndex(3,k,i))];
                        
                        opt.collisionAvoidConst = [opt.collisionAvoidConst,...
                            -CT.polyColSet.P{i,j,k}*coordDifference <=-CT.polyColSet.q{i,j,k}+opt.BigM*(ones(agent(j).Ncs,1)-opt.B_col(opt.B_colIndex(:,i,k,j)))+opt.BigM*ones(agent(j).Ncs,1)*sum(opt.B_hor(1:k-1))];

                  
                        % Constraint on binary variables (11r)
                        opt.BinColAvoidConst = [opt.BinColAvoidConst; sum(opt.B_col(opt.B_colIndex(:,i,k,j))) >= 1];
                        
                        
                    %    Avoid intersample collisions between agent
                            previousCoordDifference = [opt.X(opt.xIndex(1,k-1,j));opt.X(opt.xIndex(3,k-1,j))] - [opt.X(opt.xIndex(1,k-1,i));opt.X(opt.xIndex(3,k-1,i))];

      opt.AvoidInterSampleCollConst = [opt.AvoidInterSampleCollConst,...
                                -CT.polyColSet.P{i,j,k-1}*previousCoordDifference <=-CT.polyColSet.q{i,j,k-1}+opt.BigM*(ones(agent(j).Ncs,1)-opt.B_col(opt.B_colIndex(:,i,k,j)))+opt.BigM*ones(agent(j).Ncs,1)*sum(opt.B_hor(1:k-1))];

                    end
                end
            end
            
            % Calculate constraint size (for report purposes)
            % opt.nConst.colAvoid = agent(j).Ncs*length(opt.collisionAvoidConst) + length(opt.BinColAvoidConst);
            
        end
        %------------------------------------------------------------------
        function BuildTerminalTargetHorizonConstraints(opt,mission,CT)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method builds the opt.constraints related to the terminal
            % set and optimal horizon. The following opt.constraints are
            % implemented: (11k), (11m), (11n), (11o).
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % Target set constraint (11k)
            opt.TerminalSetConst = [];
            for j=1:mission.Na
                for k=1:opt.N+1
                 
                    pos_jthAgent = [opt.X(opt.xIndex(1,k,j));opt.X(opt.xIndex(3,k,j))];
                    for t=1:mission.Nt

                          opt.TerminalSetConst = [opt.TerminalSetConst,CT.polyTrgSet.P{t,j,k}*pos_jthAgent <= ...
                            CT.polyTrgSet.q{t,j,k} + ones(mission.Nts,1)*opt.BigM*(1-opt.B_target(opt.B_targetIndex(t,k,j)))];
                    end
                end
            end
            
            % The sum of all horizon binaries must be one (11m).
            opt.B_horConst = [sum(opt.B_hor(1:end)) == 1];
            
            % For each target, the sum of the binaries of all agent for all
            % times must be less equal than 1. This way, only one (or none)
            % agent will reach each target (11n).
            opt.B_targetConst = [];
            for t=1:mission.Nt
                
                for j=1:mission.Na
                    aux(j) = sum(opt.B_target(opt.B_targetIndex(t,1:end,j))); % sum of all agents 't'th target binary at time step j
                end
                if t < mission.Nt %
                    opt.B_targetConst = [opt.B_targetConst, sum(aux)  <= 1 - opt.wasTargetVisited(t)]; % by setting the LHS as 0, no binary will switch to 1 because of a target already visited
                else % if last target, it was not visited
                    opt.B_targetConst = [opt.B_targetConst, sum(aux) <= 1];
                end
            end
            
            % This opt.constraints prevents the agent from keep pursuing targets
            % after the optimal horizont is reached (11o). This is
            % important because the opt.constraints are relaxed after this
            % horizont, and visiting targets gets very easy. Therefore,
            % the MPC can 'decide' to visit targets after the horizon to
            % minimize the cost, and this will interfere with the results
            % before the optimal horizont is reached.
            opt.Visitb4HorizontConst = [];
            for j=1:mission.Na
                for t=1:mission.Nt
                    opt.Visitb4HorizontConst = [opt.Visitb4HorizontConst,...
                        [opt.N+1]*opt.B_target(opt.B_targetIndex(t,:,j)) <= [opt.N+1]*opt.B_hor(1:end)];
                end
            end
            
            % In this constraint we equal the sum of the binaries of the
            % last target of all agent for each   instant 'i' to the horizont
            % binary at the correspondent instant 'i' (11p). This way,
            % since b_hor must be equal to 1 at some time instant, it
            % forces b_target to be equal to 1 at the same instant.
            % Consequently, this forces one agent to reach the terminal
            % set during the horizont.
            opt.TargetHorEqualityConst  = [];
            for k=1:opt.N+1
                opt.TargetHorEqualityConst = [opt.TargetHorEqualityConst, ...
                    opt.B_hor(k)== sum(opt.B_target(opt.B_targetIndex(mission.Nt,k,:)))
                    ];
            end
            
            
            % This opt.constraints decreases the fixed horizont at each
            % time-step by one. This is necessary since we are working with
            % suboptimal results where there is no guarantee that the
            % optimal horizont will decrease at each instant.
            %             opt.decreasingTimeConst = [];
            %             opt.decreasingTimeConst = [1:opt.N+1]*opt.B_hor + opt.current_k <= opt.N;
            
            % Calculate constraint size (for report purposes)
            opt.nConst.Target = length(opt.B_horConst) + mission.Nts*length(opt.TerminalSetConst)...
                + length(opt.B_targetConst) + length(opt.TargetHorEqualityConst)...
                + length(opt.Visitb4HorizontConst);
        end

       
        %------------------------------------------------------------------
        function BuildConnectivityConstraints(opt,mission,agent,CT)
            
            % This constraint defines the connection regions (11l)
            opt.conRegionConst = [];
            for j=1:mission.Na-1
                for k=3:opt.N+1
                    for i=j+1:mission.Na
                        coordDifference = [opt.X(opt.xIndex(1,k,j));opt.X(opt.xIndex(3,k,j))] - [opt.X(opt.xIndex(1,k,i));opt.X(opt.xIndex(3,k,i))];
                        opt.conRegionConst = [opt.conRegionConst,...
                            CT.polyConnSet.P{i,j,k}*coordDifference <=  ...
                            CT.polyConnSet.q{i,j,k}+ ones(agent(j).Nsc,1)*opt.BigM*(1-opt.B_con(opt.B_conIndex(i,k,j)))...
                            + ones(agent(j).Nsc,1)*opt.BigM*sum(opt.B_hor(1:k-1))];
                    end
                end
            end
            
            % choose type of connectivity constraint
            switch opt.CONNECTIVITY_CONSTRAINT_TYPE
                % -------------
                % Case spanning tree constraint (afonso et al)
                case 'COMPLETE'
                    % Sum of all edges binaries (for k) must be equal to Na-1 (11t)
                    opt.edgesConst = [];
                    for k=1:opt.N+1
                        for j=1:mission.Na-1
                            aux(j) = sum(opt.B_con(opt.B_conIndex(j+1:end,k,j)));
                        end
                        edgesConst_k = [sum(aux) == mission.Na-1];
                        opt.edgesConst = [opt.edgesConst, edgesConst_k];
                    end
                    
                    
                    % Checks if there are cycles in the subgraphs of the chosen
                    % connection topology. Notice that we can ignore the cases where
                    % |S|=1,2, and Na. To generate all possibilites of |S|, the command
                    % nchoosek is used to calculate a combination (Na;nVertices), which
                    % yields all combinations of subgraphs with nVertices (comb). Then
                    % we do a combination of the vertices of these subgraphs taken 2 at
                    % a time (combComb), which yields the all pairs of connections for
                    % this particular subgraph.
                    opt.noCycleConst = [];
                    for nVertices=3:mission.Na-1
                        comb = nchoosek(1:mission.Na,nVertices);
                        for p=1:length(comb)
                            combComb = nchoosek(comb(p,:),2);
                            for k=1:opt.N+1
                                for q=1:length(combComb)
                                    aux(q,k) = sum(opt.B_con(opt.B_conIndex(combComb(q,2),k,combComb(q,1))));
                                end
                                opt.noCycleConst = [opt.noCycleConst, sum(aux(:,k)) <= nVertices-1];
                            end
                        end
                    end
                    
                    % -------------
                    % Heuristic from AFONSO ET AL.
                case 'HEURISTIC'
                    for j=1:mission.Na-1
                        for k=1:opt.N+1
                            opt.adjMatrixConst = [opt.adjMatrixConst, ...
                                sum(opt.B_con(opt.B_conIndex(j+1:end,k,j)))==1];
                        end
                    end
                    
                    % -------------
                    % Case theorem 1 connectivity constraint
                case 'ROBUST_SUMVERTICEDEGREE'
                    % Constructs the degree of each vertice at each instant
                    for k=1:opt.N+1
                        deg(1,k) = sum(opt.B_con(opt.B_conIndex(2:end,k,1)));
                        deg(mission.Na,k) = sum(opt.B_con(opt.B_conIndex(mission.Na,k,1:mission.Na-1)));
                        for j=2:mission.Na-1
                            deg(j,k) = sum(opt.B_con(opt.B_conIndex(j+1:end,k,j))) + ...
                                sum(opt.B_con(opt.B_conIndex(j,k,1:j-1)));
                        end
                    end
                    
                    % Alternative constraint on connectivity based on theorem 1. Less
                    % restrictive than the condition from the corollary.
                    opt.degreeSumConstraint = [];
                    for k=3:opt.N+1
                        for j=1:mission.Na-1
                            for i=j+1:mission.Na
                                opt.degreeSumConstraint = [opt.degreeSumConstraint,...
                                    deg(j,k) + deg(i,k) >= mission.Na-opt.B_con(opt.B_conIndex(i,k,j))*mission.Na-mission.Na*sum(opt.B_hor(1:k-1))];
                            end
                        end
                    end
                    
                    % -------------
                    % Case corollary 1 connectivity constraint
                case 'ROBUST_MINVERTICEDEGREE'
                    
                    % Constructs the degree of each vertice at each instant
                    for k=1:opt.N+1
                        deg(1,k) = sum(opt.B_con(opt.B_conIndex(2:end,k,1)));
                        deg(mission.Na,k) = sum(opt.B_con(opopt.B_cont.B_conIndex(mission.Na,k,1:mission.Na-1)));
                        for j=2:mission.Na-1
                            deg(j,k) = sum(opt.B_con(opt.B_conIndex(j+1:end,k,j))) + ...
                                sum(opt.B_con(opt.B_conIndex(j,k,1:j-1)));
                        end
                    end
                    
                    % Alternative connectivity constraint (Corollary from theorem 1)
                    opt.minDegreeConstraint = [];
                    for k=1:opt.N+1
                        for j=1:mission.Na
                            opt.minDegreeConstraint = [opt.minDegreeConstraint,deg(j,k)>= (mission.Na)/2];
                        end
                    end
            end
            
        end
        %------------------------------------------------------------------
        function BuildAbsUConstraint(opt)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method builds a constraint used to create the abs(u)
            % variable. There is a trick to allow us to minimize the
            % absolute value of the control signal without turning the
            % cost nonlinear. The following is done:
            % We minimize abs(u) s.t. the contraint
            % -abs(u) <= u <= abs(u).
            % Consequently, abs(u) is minimized while a value of u, which
            % respects the other opt.constraints, can still be found within.
            % The value of u can go either way, but it is always chosen as
            % one of the extremes -abs(u) or abs(u) (to be optimal). Since
            % the extremes are always chosen as u, their values are always
            % the absolute value of the optimal control signal (yes, this
            % is confusing).
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            opt.AbsUConst = [-opt.abs_U <= opt.U <= opt.abs_U];
            
            % Calculate constraint size (for report purposes)
            opt.nConst.absU = length(opt.AbsUConst);
        end
        %------------------------------------------------------------------
        function BuildPlannerConstraints(opt,agent,env,mission,CT)
           
            % This method builds the full constraint object. This procedure
            % used to be done in the main code, but due to the necessity of
            % rebuilding the problem after a failure, it was moved here.
            % This is only for the PLANNER MODE.
          
            opt.BuildInitialCondConst(mission);
            
            % Builds agent dynamics constraints
            opt.BuildDelayedDynamicConst(agent,mission);
            
            % Builds control and state opt.constraints
            opt.BuildStateControlConstraints(agent,mission,CT);
            
            % Builds obstacle avoidance opt.constraints
            opt.BuildObstacleAvoidanceConstraints(env,mission,agent,CT);
           
            % Builds terminal set and optimal horizont opt.constraints
            opt.BuildTerminalTargetHorizonConstraints(mission,CT);
            
            if mission.Na > 1    
                % Builds connectivity opt.constraints
                opt.BuildConnectivityConstraints(mission,agent,CT);
                
                
                % Builds collision avoidance opt.constraints
                opt.BuildCollisionAvoidanceConstraints(mission,agent,CT);           
            end
            
            opt.plannerConstraints = [
                opt.iniCondConst
                opt.delayedDynConst_X
                opt.stateConst
                opt.inputConst
                opt.B_horConst
                opt.TerminalSetConst
                opt.B_targetConst
                opt.Visitb4HorizontConst
                opt.TargetHorEqualityConst
               opt.ObstAvoidConst
               opt.BinObstAvoidConst
               opt.AvoidCuttingObsConst
               opt.collisionAvoidConst
               opt.BinColAvoidConst
                opt.AvoidInterSampleCollConst
                opt.degreeSumConstraint
                opt.conRegionConst
                ];
           
        end
        %------------------------------------------------------------------
        function BuildPlannerObjective(opt,mission)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method builds the cost function of the optimization
            % problem. See equation (9) for more details.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            summ_j = 0;
            % Sum all targets visits except the last one.
            for t=1:mission.Nt-1
                for j=1:mission.Na
                    aux = sum(opt.B_target(opt.B_targetIndex(t,:,j)));
                    summ_j = summ_j + aux;
                end
            end

             quadCost = 0;
             for z=1:mission.Na
                 for k=1:opt.N
                     quadCost = quadCost + transpose(opt.U(opt.uIndex(:,k,z)))*opt.gamma*opt.U(opt.uIndex(:,k,z));
                 end
             end

            opt.plannerObjective = quadCost + [1:opt.N+1]*opt.B_hor-summ_j*opt.targetReward(1);
            
        end
        %------------------------------------------------------------------
        function preOptimize(opt)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method runs the pre-optimization and creates a planner
            % object. This speeds up the optimization in the closed-loop.
            % It is particulary necessary if the 'implies' command is used.
            % Two objects are created for the two modes: Planner and Reco..
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % Mission planner object
            opt.Planner = optimizer(opt.plannerConstraints,opt.plannerObjective,opt.solverOptions,{opt.x_fb,opt.wasTargetVisited},opt.solverOptions.outputs);
            
        end
        %------------------------------------------------------------------
        function decisions = GetDecisions(opt,sol,mission)
           % this method outputs the targets visited and which agents visited 
           % which targets in the current MPC solution
           
            
            btar = sol{5};
            tarIn = opt.B_targetIndex;
            
            
            decisions = [];
            for t=1:mission.Nt
                for j=1:mission.Na
                        
                    
                    if round(sum(btar(tarIn(t,:,j))),1)
                        
                        k = find(round(btar(tarIn(t,:,j)),1)==1);
                        if isempty(k)
                            a=1;
                        end
                       decisions = [decisions ;t, k, j];
                    end
                end
            end
           
           
            
            
        end
        %------------------------------------------------------------------
        function x_k = BuildFullStateVector(opt,agent,k)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method builds a full state vector for the MAS. It
            % concatenates the state vectors of all agent, creating a
            % vector x_k with N_a columns. This full vector is used as the
            % initial condition for the optimization at k. Notice that this
            % do not updates with the failure of and agent. It has to be
            % this way because the feedback opt.x0 cannot change size, even
            % if the problem is rebuilt. Why? I dont know, it simply doesnt
            % work.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % Preliminaries
            x_k = zeros(agent(1).nx,length(agent));
            
            % Build full vector
            for j=1:length(agent)
                x_k(:,j) = agent(j).x(:,k);
            end
        end
        %------------------------------------------------------------------
        function [sol,isThereSolution] = Optimize(opt,x_fb,wasTargetVisited)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method runs the optimization using the options defined
            % in the properties of this class. For example, if
            % solverOptions.usex0=1, then the solution obtained in k-1 is
            % used as initial solution for the optimization at k. There is
            % code for warm-start as well.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % CURRENTLY BUGGED!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            % Gurobi says i'm feeding a infactible solution even when im
            % sure its factible.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
            
            % Calls optimizer object to solve problem
            opt_start = tic;
            [sol,errorcode,~,~,opt.Planner] = opt.Planner(x_fb,wasTargetVisited);
            optTime = toc(opt_start);
            
            
            % This is a flag to see if there is any solutions;
            % if there is a solution, the sum of the B_hor is equal to one
            % this flag is also used to stop the loop
            isThereSolution = sum(sol{3}(2:end)) ;
            
            
            % Show optimization diagnostics
            % OBS: this is a fix for a limitation of yalmip error codes.
            % Error code 3 states that the max iterations or time-limit was
            % reached. However, there is no distinction if a solution was
            % found or not; either way it is error code 3. Therefore, I use
            % the B_hor (sol{5}) variable to see if there is a solution. If
            % the sum of all horizont binaries is zero, there is no
            % solution.
            if opt.solverOptions.showRealTimeDiagnostics
                diag = [];
                if errorcode == 3 && sum(sol{3}) == 0
                    diag = [diag;"-> Status: No opt.conRegionConst,...solution found within the time-limit"];
                elseif errorcode == 3 && sum(sol{3}) == 1
                    diag = [diag;"-> Status: Suboptimal solution found"];
                else
                    diag = [diag;"-> Status: " + yalmiperror(errorcode)];
                end
                disp(diag + " in " + optTime + "s");
            end
        end
        %------------------------------------------------------------------
        function [waypoints,velocities,accelerations,Nopt] = ExtractTrajectories(opt,sol,mission)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Reorganizes the solution from the solver into the workable
            % variables: waypoints, velocities, and accelerations.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % Extract solutions
            Xopt = sol{1};
            Uopt = sol{2};
            B_hor_opt = sol{3};
            
            % Optimal horizon
            Nopt = find(round(B_hor_opt,1) == 1);
            
            % Pre-allocations
            waypoints = zeros(2,Nopt,mission.Na);
            velocities = zeros(2,Nopt,mission.Na);
            accelerations = zeros(2,Nopt,mission.Na);
            
            % Trajectory is the waypoints and corresponding velocities
            for j=1:mission.Na
                for k=1:Nopt
                    
                    waypoints(:,k,j) = [Xopt(opt.xIndex(1,k,j));
                        Xopt(opt.xIndex(3,k,j))];
                    
                    velocities(:,k,j) = [Xopt(opt.xIndex(2,k,j));
                        Xopt(opt.xIndex(4,k,j))];
                end
                for k=1:Nopt-1 % Nopt=prediction horizon; Nopt-1=ctrl hor.
                    accelerations(:,k,j) = Uopt(opt.uIndex(:,k,j));
                end
                
            end
        end
        %------------------------------------------------------------------
        function [posCommands,velCommands,accCommands] = BuildTrajectoriesCommandsOpenLoop(opt,traj,agent,ag_i)
            
            % first traj opt hor
            N_oploop = traj.optimalHorizon+1;
            
            % initializations
            jfinal = 60; % 60 steps
            state = zeros(length(agent.Ad_60hz),jfinal*opt.N+1);
            
            % double integrator ini cond.
            state(:,1) = [traj.waypoints(1,1,ag_i);traj.velocities(1,1,ag_i);
                traj.waypoints(2,1,ag_i);traj.velocities(2,1,ag_i)];
            
            accCommands = zeros(2,jfinal*N_oploop);
            % dynamic loop calculates pos and vel commands
            for i=1:N_oploop
                for j=1:jfinal
                    jindex = (i-1)*jfinal+j;
                    
                    accCommands(:,jindex) = traj.accelerations(:,i,ag_i);
                    % DI dinamics with T=1/60
                    state(:,jindex+1) = agent.Ad_60hz*state(:,jindex)...
                        + agent.Bd_60hz*accCommands(:,jindex);
                    
                end
            end
            % Resulting commands
            posCommands = [state(1,:);state(3,:)];
            velCommands = [state(2,:);state(4,:)];
            
        end
        %------------------------------------------------------------------
        function [posCommands,velCommands,accCommands] = BuildTrajectoriesCommands(opt,traj,agent,ag_i,delayed_u)
            
            if opt.delayedInputFlag
            acc = delayed_u(:,ag_i);
            else
               acc = traj.accelerations(:,1,ag_i);
            end
            states(1,1) = traj.waypoints(1,1,ag_i);
            states(2,1) = traj.velocities(1,1,ag_i);
            states(3,1) = traj.waypoints(2,1,ag_i);
            states(4,1) = traj.velocities(2,1,ag_i);
       
                    % Zero-order hold for acceleration
                    for k=1:60
                       accCommands(:,k) = acc;
                       states(:,k+1) = agent.Ad_60hz*states(:,k) + agent.Bd_60hz*accCommands(:,k);
                       
                       posCommands(:,k+1) = [states(1,k+1);states(3,k+1)];
                       velCommands(:,k+1) = [states(2,k+1);states(4,k+1)];
                       
                    end

            
        end
        %------------------------------------------------------------------
        function isMissionOver = CheckTargetVisitation(opt,agent,mission,timeStep,CT)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method check which targets were already visited. This is
            % used in two ways: first, if a target was visited at some
            % instant, this information is saved in a vector and then used
            % as input for the optimization in the next iteration. This
            % updates the optimization to ignore this target. Second, if
            % the last target is visited, a flag is updated and posteriorly
            % used to break the loop in the main code.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            for j=1:mission.Na
                for t=1:mission.Nt
                    if ~opt.visitedTargets(t)
                        opt.visitedTargets(t) =...
                            all(CT.polyTrgSet.P{t,j,1}*agent(j).y(:,timeStep+1)...
                            <= CT.polyTrgSet.q{t,j,1});
                    end
                end
            end
            
            % if last target was visited, updates end mission flag
            isMissionOver = opt.visitedTargets(mission.Nt);
        end

    end
end