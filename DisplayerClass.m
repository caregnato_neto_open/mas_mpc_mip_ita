classdef DisplayerClass < handle
    % This class handles everything related to data presentation; from plots to
    % printing reports.
    
    % Variable list
    % y,x,u,abs_u - output, state, control, and absolute value of the control
    % opt_N - optimal horizont
    % N - fixed horizont
    % solverTime - time spent by the solver in this optimization
    % optObj - optimal value of the objective function
    % nVisitedTargets - number of targets visited
    % x = [rx,vx,ry,vy]
    % u = [ax,ay]
    % Colors - color vector for the plots
    % LineWidth - plot line configuration
    
    properties
        
        Data = [];
        Colors = [];
        terminalInstant_k = [];
        terminalInstant_j = [];
        Ts_motorLoop =[];
        Ts_posCtrlLoop = [];
        Ts_plannerLoop = [];
        terminalStep_motorLoop = [];
        terminalStep_posCtrlLoop = [];
        fieldLimits_x = [];
        fieldLimits_y = [];
        maxPlotLength = [];
    end
    
    methods
        
        
        function PlotCollisions(plt,colTable,agent)
            
            robotLength = 0.074; 
            
            for i=1:plt.Data.Na_orig-1
                for j=i:plt.Data.Na_orig
                    for k=1:plt.Data.terminalStep_k
                        
                        if colTable(i,j,k)
                            colPos_i = [agent(i).x(1,k);agent(i).x(3,k)];
                            plot(colPos_i(1),colPos_i(2),'rx');
                            
                            
                            
                            vert_1 = colPos_i + [-robotLength/2;robotLength/2];
                            vert_2 =  colPos_i + [robotLength/2;robotLength/2];
                            vert_3 =  colPos_i + [robotLength/2;-robotLength/2];
                            vert_4 =  colPos_i + [-robotLength/2;-robotLength/2];
                            
                            vert = [vert_1,vert_2,vert_3,vert_4]';
                            
                            fill(vert(:,1),vert(:,2),plt.Colors(i,:));
                            
                            colPos_j = [agent(j).x(1,k);agent(j).x(3,k)];
                            %plot(colPos_i(1),colPos_i(2),'rx');
                            
                            vert_1 = colPos_j + [-robotLength/2;robotLength/2];
                            vert_2 =  colPos_j + [robotLength/2;robotLength/2];
                            vert_3 =  colPos_j + [robotLength/2;-robotLength/2];
                            vert_4 =  colPos_j + [-robotLength/2;-robotLength/2];
                            
                            vert = [vert_1,vert_2,vert_3,vert_4]';
                            
                            fill(vert(:,1),vert(:,2),plt.Colors(j,:));               
                            
                        end
                        
                    end
                end
                
            end 
          
        end
        
        function PlotScenario(plt)
            
            figure(); hold on; grid on;
            plt.PlotObstacles();
            plt.PlotTargets();
            plt.PlotWalls();
        end
        
        function InitializeDisplayerParameters(plt,env)
            % Limits of the field, used to build walls and determine the
            % length of the plots.
            plt.fieldLimits_x = [env.opRegionCenter(1)-env.opRegionLength/2,
                env.opRegionCenter(1)+env.opRegionLength/2] ;
            
            plt.fieldLimits_y = [env.opRegionCenter(2)-env.opRegionWidth/2,
                env.opRegionCenter(2)+env.opRegionWidth/2];
            
            % Axes size are equal to maintain scale
            plt.maxPlotLength = max(max(plt.fieldLimits_x),max(plt.fieldLimits_y)) + 0.05;
            
        end
        % -------------------------------------------------------------------------
        function PrintScenarioInfo(plt,mission,MAS)
            
            disp("-> Simulation with " + int2str(mission.Na) + " " + MAS.TYPE(1));
            for i=1:mission.Na
                disp("-> Initial position of " + int2str(i) + "th agent: " +...
                    "(" + num2str(mission.x0(1,i),"%.2f") + "," +num2str(mission.x0(3,i),"%.2f") +")");
            end
            
        end
        
        % -------------------------------------------------------------------------
        function InitializeData(plt,missionData)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method initializes general variables which will be
            % used in the remaining methods.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            plt.Data = missionData;
            plt.Ts_plannerLoop = plt.Data.MAS.Ts_plannerLoop;
            plt.Ts_posCtrlLoop = plt.Data.MAS.Ts_posCtrlLoop;
            plt.Ts_motorLoop = plt.Data.MAS.Ts_motorLoop;
        end
        % -------------------------------------------------------------------------
        function GeneratePlotColors(plt,mission)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method creates a vector of colors; each one of these
            % colors is associted with one plt.Data.agent. Therefore, in each plot,
            % the data corresponding to a plt.Data.agent always have the same color.
            % Additionally, we sample the pallet 'jet' (see matlab help)
            % with equal intervals to secure distinct enough colors. There
            % are 256 colors in total, the sampling occurs at every
            % 256/Na colors. Round is used to garantee a integer.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % Pre-allocation
            plt.Colors = zeros(mission.Na,3);
            
            % Assign pallet of colors to variable
            c = jet;
            
            % First plt.Data.agent is always assing to the color in the first index
            plt.Colors(1,:) = c(1,:);
            
            
            % Assign colors to the remaining
            for i=2:mission.Na
                plt.Colors(i,:) = c(i*floor(length(c)/mission.Na),:);
            end
        end
        % -------------------------------------------------------------------------
        function inspectEnvironment(plt,env,mission,CT)
        
            figure; hold on; grid on;
           

            for v = 1:env.No
                plot(env.polyOrigObs{v},'color','red');
            end
            
            % plot tar
            for v = 1:mission.Nt
                fill(mission.Vert_trg{v}(:,1),mission.Vert_trg{v}(:,2),[1 1 1]);
            end
            
            % orig obs
                          upperRightDiagLineParam = inv([(env.fieldLength/2-env.fieldDiagLength) 1;
                        env.fieldLength/2 1])*[env.fieldWidth/2;env.fieldWidth/2-env.fieldDiagWidth];
                    
                    lowerRightDiagLineParam = inv([(env.fieldLength/2-env.fieldDiagLength) 1;
                        env.fieldLength/2 1])*-[env.fieldWidth/2;env.fieldWidth/2-env.fieldDiagWidth] ;
                    
                    lowerLeftDiagLineParam = inv([-(env.fieldLength/2-env.fieldDiagLength) 1;
                        -env.fieldLength/2 1])*-[env.fieldWidth/2;env.fieldWidth/2-env.fieldDiagWidth] ;
                    
                    upperLeftDiagLineParam = inv([-(env.fieldLength/2-env.fieldDiagLength) 1;
                        -env.fieldLength/2 1])*[env.fieldWidth/2;env.fieldWidth/2-env.fieldDiagWidth]  ;
                    
                    polyPosSet.P = [ eye(2);
                        -eye(2);
                        1 upperRightDiagLineParam(1);
                        -1 -lowerRightDiagLineParam(1);
                        -1 -lowerLeftDiagLineParam(1);
                        1 upperLeftDiagLineParam(1)];
                    
                    polyPositionSet.q= [env.fieldLength/2;
                        env.fieldWidth/2;
                        env.fieldLength/2;
                        env.fieldWidth/2
                        upperRightDiagLineParam(2);
                        -lowerRightDiagLineParam(2)
                        -lowerLeftDiagLineParam(2)
                        upperLeftDiagLineParam(2)];
           
           
           
           fieldPoly =Polyhedron(polyPosSet.P, polyPositionSet.q);
           %fieldPolyNom = minHRep(fieldPoly);
           
           fieldPoly.plot('alpha',0,'color','black')
            fieldPolyTight = Polyhedron(CT.polyPositionSet.P{1,1},CT.polyPositionSet.q{1,1});
            fieldPolyTight.plot('alpha',0,'color','black');
          % plot(fieldPoly,'alpha',0,'color','black');
           xlim([-0.8 0.8]); ylim([-0.8 0.8]);
           
%             plot([wallLimit_x(1),wallLimit_x(1)],[wallLimit_y(1),wallLimit_y(2)],'k','LineWidth',1);
%             plot([wallLimit_x(2),wallLimit_x(2)],[wallLimit_y(1),wallLimit_y(2)],'k','LineWidth',1);
%             plot([wallLimit_x(1),wallLimit_x(2)],[wallLimit_y(1),wallLimit_y(1)],'k','LineWidth',1);
%             plot([wallLimit_x(1),wallLimit_x(2)],[wallLimit_y(2),wallLimit_y(2)],'k','LineWidth',1);
%             
        end
        
        
        
        
        
        % -------------------------------------------------------------------------
        function PlotObstacles(plt)
            
            for v = 1:plt.Data.env.No
                fill(plt.Data.env.Vert_obs{v}(:,1),plt.Data.env.Vert_obs{v}(:,2),'k');
            end
        end
        % --------------------------------------------------------------------------
        function PlotTargets(plt)
            for v = 1:plt.Data.mission.Nt
                fill(plt.Data.mission.Vert_trg{v}(:,1),plt.Data.mission.Vert_trg{v}(:,2),[1 1 1]);
            end
        end
        % --------------------------------------------------------------------------
        function PlotWalls(plt)
            % Plot walls
            wallLimit_x = [plt.Data.env.opRegionCenter(1)-plt.Data.env.opRegionLength/2,
                plt.Data.env.opRegionCenter(1)+plt.Data.env.opRegionLength/2] ;
            
            wallLimit_y = [plt.Data.env.opRegionCenter(2)-plt.Data.env.opRegionWidth/2,
                plt.Data.env.opRegionCenter(2)+plt.Data.env.opRegionWidth/2];
            
            plot([wallLimit_x(1),wallLimit_x(1)],[wallLimit_y(1),wallLimit_y(2)],'k','LineWidth',0.5);
            plot([wallLimit_x(2),wallLimit_x(2)],[wallLimit_y(1),wallLimit_y(2)],'k','LineWidth',0.5);
            plot([wallLimit_x(1),wallLimit_x(2)],[wallLimit_y(1),wallLimit_y(1)],'k','LineWidth',0.5);
            plot([wallLimit_x(1),wallLimit_x(2)],[wallLimit_y(2),wallLimit_y(2)],'k','LineWidth',0.5);
        end
        
        
        % --------------------------------------------------------------------------
        function PlotRealTrajectory(plt,CT)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method plots all trajectories, obstacles and collision
            % instants.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            figure(); hold on; grid;
            
            plt.Colors(3,:) = [110,166,48]/256;
             plt.Colors(4,:) = [232,127,51]/256;
           % plt.Data.terminalStep_k = 647;
             
            plt.PlotObstacles();
            plt.PlotTargets();
          %  plt.PlotWalls();
                      fieldPolyTight = Polyhedron(CT.polyPositionSet.P{1,1},CT.polyPositionSet.q{1,1});
            fieldPolyTight.plot('alpha',0,'color','black');
            
            % Plot real trajectory
            for j=1:plt.Data.Na_orig
                plot(plt.Data.agent(j).x(1,1:plt.Data.terminalStep_k+1),...
                    plt.Data.agent(j).x(3,1:plt.Data.terminalStep_k+1),...
                    '-','color',plt.Colors(j,:),'linewidth',1.5)
            end
            %            xlim([-plt.maxPlotLength;plt.maxPlotLength]); ylim([-plt.maxPlotLength;plt.maxPlotLength]);
            xlabel("$\mathtt{r}_x(m)$",'interpreter','latex','fontsize',16);
            ylabel("$\mathtt{r}_y(m)$",'interpreter','latex','fontsize',16);
        end
        
        % -------------------------------------------------------------------------
        function PlotVirtualTrajectories(plt,MPC,timeStep,env,mission,MAS)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method plots the virtual trajectories (calculated by the
            % MPC at each instant).
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            waypoints = MPC.trajectories(timeStep).waypoints;
            Nopt = MPC.trajectories(timeStep).optimalHorizon;
            
            figure(); hold on; grid on;
            % Plot obstacles
            for v = 1:env.No
                fill(env.Vert_obs{v}(:,1),env.Vert_obs{v}(:,2),'k');
            end
            % Plot targets
            for v = 1:mission.Nt
                fill(mission.Vert_trg{v}(:,1),mission.Vert_trg{v}(:,2),[1 1 1]);
            end
            % Plot walls
            wallLimit_x = [env.opRegionCenter(1)-env.opRegionLength/2,
                env.opRegionCenter(1)+env.opRegionLength/2] ;
            wallLimit_y = [env.opRegionCenter(2)-env.opRegionWidth/2,
                env.opRegionCenter(2)+env.opRegionWidth/2];
            plot([wallLimit_x(1),wallLimit_x(1)],[wallLimit_y(1),wallLimit_y(2)],'k','LineWidth',1.5);
            plot([wallLimit_x(2),wallLimit_x(2)],[wallLimit_y(1),wallLimit_y(2)],'k','LineWidth',1.5);
            plot([wallLimit_x(1),wallLimit_x(2)],[wallLimit_y(1),wallLimit_y(1)],'k','LineWidth',1.5);
            plot([wallLimit_x(1),wallLimit_x(2)],[wallLimit_y(2),wallLimit_y(2)],'k','LineWidth',1.5);
            
            % Plots solution trajectories from the solver for each k
            for j=1:mission.Na
                plot(waypoints(1,1:Nopt+1,j),waypoints(2,1:Nopt+1,j),...
                    '-s','color',plt.Colors(j,:),'MarkerEdgeColor',plt.Colors(j,:),...
                    'MarkerFaceColor',plt.Colors(j,:),'MarkerSize',2,'linewidth',1.5); drawnow;
                
            end
            % xlim([-plt.maxPlotLength;plt.maxPlotLength]); ylim([-plt.maxPlotLength;plt.maxPlotLength]);
            xlabel("$\mathtt{r}_x(m)$",'interpreter','latex','fontsize',16);
            ylabel("$\mathtt{r}_y(m)$",'interpreter','latex','fontsize',16);
            xlim([-0.75 0.75]); ylim([-0.65 0.65]);
            
        end
        % -------------------------------------------------------------------------
        function PlotAgentVelocities(plt)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method plots all velocities. It is automated to create a
            % subplot and divide the Na velocities within.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % Plots v_1 (v_x)
            figure(); grid;
            for j=1:plt.Data.Na_orig
                subplot(2,ceil(plt.Data.Na_orig/2),j);
                plot([0:plt.Data.terminalStep_k-1]*plt.Ts_motorLoop,...
                    plt.Data.agent(j).x(2,1:plt.Data.terminalStep_k),...
                    'color',plt.Colors(j,:),'linewidth',1.5); grid; hold;
                
                %Constraint
                yline(plt.Data.agent(j).x_MAX(2),'--m');
                yline(plt.Data.agent(j).x_MIN(2),'--m');
                ylim([-1.3 1.3]);
                
                ylabel("$\mathtt{v}_x(m/s)$",'interpreter','latex','fontsize',16);
                xlabel('$\mathtt{Time}(s)$','interpreter','latex','fontsize',16);
                title("Agent " + num2str(j));
            end
            
            % Plots v_2 (v_y)
            figure(); grid;
            for j=1:plt.Data.Na_orig
                subplot(2,ceil(plt.Data.Na_orig/2),j);
                plot([0:plt.Data.terminalStep_k-1]*plt.Ts_motorLoop,...
                    plt.Data.agent(j).x(4,1:plt.Data.terminalStep_k),...
                    'color',plt.Colors(j,:),'linewidth',1.5); grid; hold;
                
                % Constraint
                yline(plt.Data.agent(j).x_MAX(4),'--m');
                yline(plt.Data.agent(j).x_MIN(4),'--m');
                ylim([-1.3 1.3]);
                
                ylabel("$\mathtt{v}_y(m/s)$",'interpreter','latex','fontsize',16);
                xlabel('$\mathtt{Time}(s)$','interpreter','latex','fontsize',16);
                title("Agent " + num2str(j));
            end
            
        end
        % -------------------------------------------------------------------------
        function PlotWheelVelocities(plt,tf)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method plots all control signals. It is automated
            % to create a subplot and divide the Na velocities within.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            
            % Time window
            if isempty(tf)
                finalStep_k = plt.Data.terminalStep_k;
                finalStep_j = plt.Data.terminalStep_j;
            else
                finalStep_k = tf/plt.Ts_motorLoop;
                finalStep_j = tf/plt.Ts_posCtrlLoop;
            end
            
            % --RIGHT WHEEL VELOCITY
            figure();
            for j=1:plt.Data.Na_orig
                subplot(2,ceil(plt.Data.Na_orig/2),j);
                plot([0:finalStep_k-1]*plt.Ts_motorLoop,...
                    plt.Data.agent(j).motor.quantizedWheelVel(1,1:finalStep_k),...
                    'color','k','linewidth',1); grid on; hold on;
                
                plot([0:finalStep_j-1]*plt.Ts_posCtrlLoop,...
                    plt.Data.agent(j).posCtrl.wheelVelCommand(1,1:finalStep_j),...
                    'r--','LineWidth',1.5);
                
                
                ylabel("$\omega_R(rad/s)$",'interpreter','latex','fontsize',16);
                xlabel('$\mathtt{Time}(s)$','interpreter','latex','fontsize',16);
                title("Agent " + num2str(j));
                legend('\omega_R','Command');
            end
            
            % --LEFT WHEEL VELOCITY
            figure();
            for j=1:plt.Data.Na_orig
                subplot(2,ceil(plt.Data.Na_orig/2),j);
                plot([0:finalStep_k-1]*plt.Ts_motorLoop,...
                    plt.Data.agent(j).motor.quantizedWheelVel(2,1:finalStep_k),...
                    'color','k','linewidth',1);  grid on; hold on;
                
                plot([0:finalStep_j-1]*plt.Ts_posCtrlLoop,...
                    plt.Data.agent(j).posCtrl.wheelVelCommand(2,1:finalStep_j),...
                    'r--','LineWidth',1.5);
                
                ylabel("$\omega_L(rad/s)$",'interpreter','latex','fontsize',16);
                xlabel('$\mathtt{Time}(s)$','interpreter','latex','fontsize',16);
                title("Agent " + num2str(j));
                legend('\omega_L','Command');
            end
            
        end
        % -------------------------------------------------------------------------
        function PlotUnicycleVelocities(plt)
            
            % lin vel
            figure();
            for n=1:plt.Data.mission.Na
                subplot(2,ceil(plt.Data.mission.Na/2),n);
                grid on; hold on;
                plot([0:plt.Data.terminalStep_k-1]*plt.Ts_motorLoop,...
                    plt.Data.agent(n).linVel(1:plt.Data.terminalStep_k),...
                    'color','k','LineWidth',1.5);
                
                
                plot([0:plt.Data.terminalStep_j-1]*plt.Ts_posCtrlLoop,...
                    plt.Data.agent(n).linVelCommand(1:plt.Data.terminalStep_j),...
                    'r--','LineWidth',1.5);
                
                ylabel("$\mathtt{Linear}\ \mathtt{Velocity}$",'interpreter','latex','fontsize',12);
                xlabel('$\mathtt{Time}(s)$','interpreter','latex','fontsize',12);
                title("Agent " + num2str(n));
                xlim([0,plt.Data.terminalStep_j-1]*plt.Ts_posCtrlLoop);
                legend('Linear Vel.','Command');
                
            end
            
            % ang vel
            figure();
            for n=1:plt.Data.mission.Na
                subplot(2,ceil(plt.Data.mission.Na/2),n);
                grid on; hold on;
                plot([0:plt.Data.terminalStep_k-1]*plt.Ts_motorLoop,...
                    plt.Data.agent(n).angVel(1:plt.Data.terminalStep_k),...
                    'color','k','LineWidth',1.5);
                
                plot([0:plt.Data.terminalStep_j-1]*plt.Ts_posCtrlLoop,...
                    plt.Data.agent(n).angVelCommand(1:plt.Data.terminalStep_j),...
                    'r--','LineWidth',1.5);
                
                ylabel("$\mathtt{Angular}\ \mathtt{Velocity}$",'interpreter','latex','fontsize',12);
                xlabel('$\mathtt{Time}(s)$','interpreter','latex','fontsize',12);
                title("Agent " + num2str(n));
                xlim([0,plt.Data.terminalStep_j-1]*plt.Ts_posCtrlLoop);
                legend('Angular Vel.','Command');
            end
            
            
        end
        % -------------------------------------------------------------------------
        function PlotAgentInputs(plt)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method plots agent inputs. This is used specifically
            % when the double integrator model is the plant.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % u_1
            figure();
            for j=1:plt.Data.mission.Na
                subplot(2,ceil(plt.Data.mission.Na/2),j);
                plot([0:plt.Data.terminalStep_i-1]*plt.Ts_plannerLoop,...
                    plt.Data.agent(j).u(1,1:plt.Data.terminalStep_i),'b','linewidth',1.5);
                grid on; hold on;
                ylabel("$u_1$",'interpreter','latex','fontsize',12);
                xlabel('$\mathtt{Time}(s)$','interpreter','latex','fontsize',12);
                title("Agent " + num2str(j));
                yline(plt.Data.agent(j).u_MAX(1),'--m');
                yline(plt.Data.agent(j).u_MIN(1),'--m');
                ylim([plt.Data.agent(j).u_MIN(1)-0.05,plt.Data.agent(j).u_MAX(1)+0.05]);
                xlim([0,plt.Data.terminalStep_i-1]*plt.Ts_motorLoop);
                hold off;
            end
            
            % u_2
            figure();grid;
            for j=1:plt.Data.mission.Na
                subplot(2,ceil(plt.Data.mission.Na/2),j);
                plot([0:plt.Data.terminalStep_i-1]*plt.Ts_plannerLoop,...
                    plt.Data.agent(j).u(2,1:plt.Data.terminalStep_i),'b','linewidth',1.5);
                grid on;  hold on;
                ylabel("$u_2$",'interpreter','latex','fontsize',16);
                xlabel('$\mathtt{Time}(s)$','interpreter','latex','fontsize',16);
                title("Agent " + num2str(j));
                yline(plt.Data.agent(j).u_MAX(2),'--m');
                yline(plt.Data.agent(j).u_MIN(2),'--m');
                ylim([plt.Data.agent(j).u_MIN(2)-0.05,plt.Data.agent(j).u_MAX(2)+0.05]);
                xlim([0,plt.Data.terminalStep_i-1]);
                hold off;
            end
            
            
        end
        % -------------------------------------------------------------------------
        function PlotMotorVoltages(plt,tf)
            
            % Time window
            if isempty(tf)
                finalStep = plt.Data.terminalStep_k;
            else
                finalStep = tf/plt.Ts_motorLoop;
            end
            
            figure();
            for j=1:plt.Data.Na_orig
                subplot(2,ceil(plt.Data.Na_orig/2),j);
                % --RIGHT WHEEL VOLTAGE
                grid on; hold on;
                plot([0:finalStep-1]*plt.Ts_motorLoop,...
                    plt.Data.agent(j).motor.voltages(1,1:finalStep),...
                    'k','linewidth',1);
                
                % --LEFT WHEEL VOLTAGE
                plot([0:finalStep-1]*plt.Ts_motorLoop,...
                    plt.Data.agent(j).motor.voltages(2,1:finalStep),...
                    'r--','linewidth',1);
                
                yline(8.3,'--m');
                yline(-8.3,'--m');
                ylim([-9 9]);
                
                ylabel("$\mathtt{Voltages}(V)$",'interpreter','latex','fontsize',12);
                xlabel('$\mathtt{Time}(s)$','interpreter','latex','fontsize',12);
                title("Agent " + num2str(j));
                legend('V_R','V_L');
            end
            
        end
        % -------------------------------------------------------------------------
        function PlotGraphs(plt)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method plots the communication network graphs for each
            % instant.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            nColumnsPlot = 5; % Number of columns in the subplot
            iStep = 1; % plots graphs for i=1,(1+ iStep),(1+2iStep)....
            i = 0; % auxiliary for subplot index
            
            %LOSflag = plt.Data.MAS.LOS;
            LOSflag = 0;
            
            if LOSflag
                graphToPlot = plt.Data.graphsPhysWithLOS;
            else
                graphToPlot = plt.Data.graphsPhys;
            end
            
            figure();
            for k=1:iStep:plt.Data.terminalStep_i
                i = i + 1;
                subplot(ceil((plt.Data.terminalStep_i/iStep)/nColumnsPlot),nColumnsPlot,i)
                %plot(plt.Data.graphsPhys{k});
                plot(graphToPlot{k});
                t = k-1;
                title("G_{phys}("+t+")");
            end
        end
        
        % -------------------------------------------------------------------------
        function PlotCutVertices(plt)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method plots number of the communication graph at each
            % instant.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % REMOVE THIS
            plt.Data.MAS.LOS = 1;
            
            figure();
            % h = plot([0:length(plt.Data.nCutVertices)-1],plt.Data.nCutVertices); grid;
            h =  stem([0:length(plt.Data.nCutVertices)-1],plt.Data.nCutVertices); grid;
            set(h,'LineWidth',1.5,'Color','k');
            ylabel("$\mathtt{N.\ of\ cut\  vertices}$",'interpreter','latex','fontsize',16);
            xlabel('$\mathtt{Time}(s)$','interpreter','latex','fontsize',16);
            ylim([-1 plt.Data.Na_orig]);
            xlim([-0.5 plt.Data.terminalStep_i-1+0.5]);
            
            %             if plt.Data.MAS.LOS
            %             hold;
            %             %h2 = plot([0:length(plt.Data.nCutVertices)-1],plt.Data.nCutVerticesLOS);
            %             h2 = stem([0:length(plt.Data.nCutVertices)-1],plt.Data.nCutVerticesLOS);
            %             set(h2,'LineWidth',1,'Color','r','Marker','o','MarkerSize',8);
            %             legend('Prox','Prox+LOS');
            %             end
        end
        % -------------------------------------------------------------------------
        function PlotUnicycleOrientation(plt)
            
            figure();
            for n=1:plt.Data.mission.Na
                subplot(2,ceil(plt.Data.mission.Na/2),n);
                grid on; hold on;
                plot([0:plt.Data.terminalStep_k-1]*plt.Ts_motorLoop,...
                    plt.Data.agent(n).xUnicycle(3,1:plt.Data.terminalStep_k)*180/pi,...
                    'color','k','LineWidth',1.5);
                
                
                ylabel("$\mathtt{Orientation}(deg)$",'interpreter','latex','fontsize',12);
                xlabel('$\mathtt{Time}(s)$','interpreter','latex','fontsize',12);
                title("Agent " + num2str(n));
                xlim([0,plt.Data.terminalStep_j-1]*plt.Ts_posCtrlLoop);
            end
            
        end
        % -------------------------------------------------------------------------
        %         function PlotTrajectoryWithConRegion(plt,kStep)
        %             %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %             % This method plots the trajectories and the communication
        %             % regions.
        %             %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %
        %             % relevant instants to plot
        %             instants = [1:kStep:2];
        %             instants = [18,21];
        %
        %             figure();
        %             nInstants = round(length(instants));
        %             nColumnsPlot =  ceil(nInstants/2) ; % Number of columns in the subplot
        %             z=0;
        %             for i=1: nInstants
        %                 figure();
        %                 z = z + 1;
        %                 % subplot(ceil((plt.Data.terminalInstant/kStep)/nColumnsPlot),nColumnsPlot,z)
        %                 hold on; grid;
        %
        %                 % Plot regions
        %                 for j=1:plt.Data.Na_orig
        %                     % calculate regions
        %                     instantPos = [plt.Data.agent(j).y(1,instants(i));plt.Data.agent(j).y(2,instants(i))];
        %                     currentVert(:,1) = plt.Data.agent(j).conRegVert(:,1) + instantPos(1);
        %                     currentVert(:,2) = plt.Data.agent(j).conRegVert(:,2) + instantPos(2);
        %
        %                     % plot regions
        %                     h =  fill(currentVert(:,1),currentVert(:,2),plt.Colors(j,:));
        %                     set(h,'FaceAlpha',0.15,'LineStyle','none');
        %
        %                     % plot center
        %                     plot(instantPos(1),instantPos(2),'o','MarkerSize',3,...
        %                         'MarkerEdgeColor',plt.Colors(j,:),'MarkerFaceColor',plt.Colors(j,:));
        %                 end
        %
        %                 % Plot obstacles
        %                 for v = 1:plt.Data.env.No
        %                     fill(plt.Data.env.Vert_obs{v}(:,1),plt.Data.env.Vert_obs{v}(:,2),'k');
        %                 end
        %
        %                 % Plot target sets
        %                 for v = 1:plt.Data.mission.Nt
        %                     fill(plt.Data.mission.Vert_trg{v}(:,1),plt.Data.mission.Vert_trg{v}(:,2),[1 1 1]);
        %                 end
        %
        %                 title("k=" + instants(i));
        %                 xlabel("$\mathtt{r}_x(m)$",'interpreter','latex','fontsize',16);
        %                 ylabel("$\mathtt{r}_y(m)$",'interpreter','latex','fontsize',16);
        %                 %                 xticks([1:10]);
        %                 %                 yticks([1:10]);
        %                 hold off;
        %             end
        %         end
        % -------------------------------------------------------------------------
        function PlotOptimalHorizonts(plt)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % This method plots the the optimal horizont calculated related
            % to the virtual trajectories at each instant.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            figure();
            %h = plot([0:length(plt.Data.N_opt)-1],plt.Data.N_opt); grid;
            h = stem([0:length(plt.Data.N_opt)-1],plt.Data.N_opt);grid;
            set(h,'LineWidth',1.3,'Color','k');
            ylabel("$\mathtt{Optimal\ horizont}$",'interpreter','latex','fontsize',16);
            xlabel('$\mathtt{Time}(s)$','interpreter','latex','fontsize',16);
            ylim([0 max(plt.Data.N_opt)+1]);
            xlim([-.5 17.5]);
            
        end
        
        % -------------------------------------------------------------------------
        function RunKinematics(plt,step)
            
            if isempty(step)
                step =1;
            end
            
            figure();
            % for k = 1:7:plt.Data.terminalStep_k+1
            %           for k = 1:1:plt.Data.terminalStep_k+1
            k = 1;
            % while k <= plt.Data.terminalStep_k+1
            while 1
                go = 0; pause(0.5);
                clf; hold on; grid on;
                
                plt.PlotObstacles();
                plt.PlotTargets();
                plt.PlotWalls()
                
                for i = 1:plt.Data.mission.Na
                    
                    % Extract info from state vector
                    rx = plt.Data.agent(i).x(1,k);
                    ry = plt.Data.agent(i).x(3,k);
                    theta = atan(plt.Data.agent(i).x(3,k)/plt.Data.agent(i).x(1,k));
                    
                    % 2D rotation matrix
                    %rotationMatrix =[cos(theta), -sin(theta); sin(theta), cos(theta)];
                    rotationMatrix = eye(2);
                    
                    % Calculate vert. pos. for the ith agent at kth instant
                    L = [plt.Data.MAS.BOT_DIMENSIONS(i,1)/2;plt.Data.MAS.BOT_DIMENSIONS(i,2)/2];
                    vRobot(:,1) = [rx;ry] + rotationMatrix*[L(1);L(2)];%
                    vRobot(:,2) = [rx;ry] + rotationMatrix*[L(1);-L(2)];%
                    vRobot(:,3) = [rx;ry] + rotationMatrix*[-L(1);-L(2)];%
                    vRobot(:,4) = [rx;ry] + rotationMatrix*[-L(1);L(2)];%
                    robotBodyCurrentVertices = [vRobot(:,1),vRobot(:,2),vRobot(:,3),vRobot(:,4)]';
                    fill(robotBodyCurrentVertices(:,1),robotBodyCurrentVertices(:,2),plt.Colors(i,:));
                    
                    
                    % Calculate
                    vConnRegion(:,1) = [rx;ry] + plt.Data.agent(i).conRegVert(1,:)';
                    vConnRegion(:,2) = [rx;ry] + plt.Data.agent(i).conRegVert(2,:)';
                    vConnRegion(:,3) = [rx;ry] + plt.Data.agent(i).conRegVert(3,:)';
                    vConnRegion(:,4) = [rx;ry] + plt.Data.agent(i).conRegVert(4,:)';
                    vConnRegion(:,5) = [rx;ry] + plt.Data.agent(i).conRegVert(5,:)';
                    vConnRegion(:,6) = [rx;ry] + plt.Data.agent(i).conRegVert(6,:)';
                    vConnRegion(:,7) = [rx;ry] + plt.Data.agent(i).conRegVert(7,:)';
                    vConnRegion(:,8) = [rx;ry] + plt.Data.agent(i).conRegVert(8,:)';
                    connRegionCurrentVertices = [vConnRegion(:,1),vConnRegion(:,2),vConnRegion(:,3),vConnRegion(:,4),...
                        vConnRegion(:,5),vConnRegion(:,6),vConnRegion(:,7),vConnRegion(:,8)]';
                    
                    % Plot ith robot
                    fill(connRegionCurrentVertices(:,1),connRegionCurrentVertices(:,2),plt.Colors(i,:),'FaceAlpha',0.05,'LineStyle','none');
                    
                    % text
                    % text(0.1,0.4,num2str(k/240));
                end
                %                xlim([-plt.maxPlotLength;plt.maxPlotLength]); ylim([-plt.maxPlotLength;plt.maxPlotLength]);
                xlabel("$\mathtt{r}_x(m)$",'interpreter','latex','fontsize',16);
                ylabel("$\mathtt{r}_y(m)$",'interpreter','latex','fontsize',16);
                
                
                drawnow;
                
                % Code to iterate plot pressing keyboard arrows
                while ~go
                    z = waitforbuttonpress;
                    switch double(get(gcf,'CurrentCharacter'))
                        case 29
                            go=1;
                            k = k+step;
                        case 28
                            k = k-step;
                            go=1;
                            display("flag");
                        otherwise
                            go=0;
                    end
                end
                
                
            end
            
        end
        % -------------------------------------------------------------------------
        function PlotPositions(plt,tf)
            
            % Time window
            if isempty(tf)
                finalStep = plt.Data.terminalStep_k;
            else
                finalStep = tf/plt.Ts_motorLoop;
            end
            
            figure();
            plot([0:finalStep-1]*plt.Ts_motorLoop,...
                plt.Data.agent.xUnicycle(1,1:finalStep)); grid;
            xlabel("$Time(m)$",'interpreter','latex','fontsize',16);
            ylabel("$\mathtt{r}_x(m)$",'interpreter','latex','fontsize',16);
            
            figure();
            plot([0:finalStep-1]*plt.Ts_motorLoop,...
                plt.Data.agent.xUnicycle(2,1:finalStep)); grid;
            xlabel("$Time(m)$",'interpreter','latex','fontsize',16);
            ylabel("$\mathtt{r}_y(m)$",'interpreter','latex','fontsize',16);
            %Qylim([0 0.11])
        end
        % -------------------------------------------------------------------------
        function PlotExperimentalData(plt,agent)
            load('expPos_wheelVel_15rad.mat');
            load('posITANDROIDS_wheelVel15.mat');
            
            posITANDROIDS_wheelVel15_x = posITANDROIDS_wheelVel15(:,2) + abs(posITANDROIDS_wheelVel15(1,2));
            % posITANDROIDS_wheelVel10_x = posITANDROIDS_wheelVel10_x(4:end);
            kf = length(posITANDROIDS_wheelVel15_x);
            elapsedTime = (1/60)*kf;
            %kfinal_exp = 2.5/(1/60);
            kfinal_240Hz = elapsedTime/(1/240);
            kfinal_60Hz = elapsedTime/(1/60);
            figure(); hold; grid;
            plot([1:kfinal_60Hz]*(1/60),(pos15rad(1:kfinal_60Hz,1)-pos15rad(1,1)),'m');
            % plot([0:kfinal_exp-1]*(1/60),(CMC10pos15rad(1:kfinal_exp,2)-CMC10pos15rad(1,2)),'k');% hold; grid;
            % plot([1:kfinal_exp]*(1/60),(shiftedPosRobot(1:kfinal_exp,1)-shiftedPosRobot(1,1)),'r'); hold; grid;
            %  plot([1:kfinal_sim]*(1/240),plt.Data.agent.xUnicycle(1,1:kfinal_sim)-plt.Data.agent.xUnicycle(1,1),'b');
            plot([1:kfinal_240Hz]*(1/240),agent.xUnicycle(1,1:kfinal_240Hz)-agent.xUnicycle(1,1),'b');
            plot([1:kfinal_60Hz]*(1/60),-posITANDROIDS_wheelVel15_x(1:kfinal_60Hz))
            
            xlabel('tempo(s)'); ylabel('r_x(m)');
            legend('Experiment','MATLAB','C++');
            % legend('Exp1','CMC10','MATLAB');
            
        end
        
        function PlotTrajectoryAndDesiredTrajectory(plt,traj)
            figure(); hold on; grid;
            plt.PlotObstacles();
            plt.PlotTargets();
            plt.PlotWalls();
            
            %terminal_step_k = (traj.optimalHorizon)*(1/plt.Data.MAS.Ts_motorLoop);
            %terminal_step_j = (traj.optimalHorizon)*(1/plt.Data.MAS.Ts_posCtrlLoop);
            
            
            % Plot real trajectory
            for j=1:plt.Data.Na_orig
                plot(plt.Data.agent(j).x(1,1:4:plt.Data.terminalStep_k),...
                    plt.Data.agent(j).x(3,1:4:plt.Data.terminalStep_k),...
                    'bo','MarkerSize',2)
            end
            
            
            
            % Plot desired trajectory
            for j=1:plt.Data.Na_orig
                plot(plt.Data.agent(j).posCtrl.posCommand(1,1:plt.Data.terminalStep_j+1),...
                    plt.Data.agent(j).posCtrl.posCommand(2,1:plt.Data.terminalStep_j+1),...
                    'ro','MarkerSize',2)
            end
            xlabel("$\mathtt{r}_x(m)$",'interpreter','latex','fontsize',16);
            ylabel("$\mathtt{r}_y(m)$",'interpreter','latex','fontsize',16);
            
            
        end
        % -------------------------------------------------------------------------
    end
end

