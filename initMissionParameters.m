function mission = initMissionParameters(missionType)

switch missionType
    
    case 'Scenario_1'
        
        % Number of agents
        mission.Na = 5;

        mission.x0(:,1) = [-0.65; 0; 0.55; 0];
        mission.x0(:,2) = [-0.65; 0; 0.35; 0];
        mission.x0(:,3) = [-0.4; 0; 0.15; 0];
        mission.x0(:,4) = [-0.4; 0; 0.55; 0];
        mission.x0(:,5) = [-0.4; 0; 0.35; 0];
        
        
        mission.ETA_s = 20;
        
        % targets Centers and lengths
        TAR_CENTER(1,:) = [0,-0.4];
        TAR_LENGTH(1,:) = [0.2,0.2];
        
        TAR_CENTER(2,:) = [0,0.3];
        TAR_LENGTH(2,:) = [0.2,0.2];
        
        TAR_CENTER(3,:) = [0.5,-0.1];
        TAR_LENGTH(3,:) = [0.2,0.2];
        
        % Number of targets
        mission.Nt = size(TAR_CENTER,1);
        
        % Polytope and vertices
        for n=1:mission.Nt
            
            % Vertices
            mission.Vert_trg{n}(1,:) = [TAR_CENTER(n,1) + TAR_LENGTH(n,1)/2,...
                TAR_CENTER(n,2) + TAR_LENGTH(n,2)/2];
            
            mission.Vert_trg{n}(2,:) = [TAR_CENTER(n,1) - TAR_LENGTH(n,1)/2,...
                TAR_CENTER(n,2) + TAR_LENGTH(n,2)/2];
            
            mission.Vert_trg{n}(3,:) = [TAR_CENTER(n,1) - TAR_LENGTH(n,1)/2,...
                TAR_CENTER(n,2) - TAR_LENGTH(n,2)/2];
            
            mission.Vert_trg{n}(4,:) = [TAR_CENTER(n,1) + TAR_LENGTH(n,1)/2,...
                TAR_CENTER(n,2) - TAR_LENGTH(n,2)/2];
            
            
        end
        
        % Number of polytope sides
        mission.Nts = length(mission.Vert_trg{1});
        
        
    case 'Scenario_2'
        
        % Number of agents
        mission.Na = 5;

        mission.x0(:,1) = [-0.65; 0; 0.55; 0];
        mission.x0(:,2) = [-0.65; 0; 0.35; 0];
        mission.x0(:,3) = [-0.4; 0; 0.15; 0];
        mission.x0(:,4) = [-0.4; 0; 0.55; 0];
        mission.x0(:,5) = [-0.4; 0; 0.35; 0];
        
        mission.ETA_s = 20;
        
        % targets Centers and lengths
        TAR_CENTER(1,:) = [-0.5325,-0.250];
        TAR_LENGTH(1,:) = [0.2,0.2];
        
        TAR_CENTER(2,:) = [-0.050,0.350];
        TAR_LENGTH(2,:) = [0.2,0.2];
        
        TAR_CENTER(3,:) = [0.200,-0.3];
        TAR_LENGTH(3,:) = [0.2,0.2];
        
        TAR_CENTER(4,:) = [0.4825,0.4];
        TAR_LENGTH(4,:) = [0.2,0.2];
        
        TAR_CENTER(5,:) = [0.5825,-0.2];
        TAR_LENGTH(5,:) = [0.2,0.2];
        
        % Number of targets
        mission.Nt = size(TAR_CENTER,1);
        
        % Polytope and vertices
        for n=1:mission.Nt
            
            % Vertices
            mission.Vert_trg{n}(1,:) = [TAR_CENTER(n,1) + TAR_LENGTH(n,1)/2,...
                TAR_CENTER(n,2) + TAR_LENGTH(n,2)/2];
            
            mission.Vert_trg{n}(2,:) = [TAR_CENTER(n,1) - TAR_LENGTH(n,1)/2,...
                TAR_CENTER(n,2) + TAR_LENGTH(n,2)/2];
            
            mission.Vert_trg{n}(3,:) = [TAR_CENTER(n,1) - TAR_LENGTH(n,1)/2,...
                TAR_CENTER(n,2) - TAR_LENGTH(n,2)/2];
            
            mission.Vert_trg{n}(4,:) = [TAR_CENTER(n,1) + TAR_LENGTH(n,1)/2,...
                TAR_CENTER(n,2) - TAR_LENGTH(n,2)/2];
            
            
        end
        
        % Number of polytope sides
        mission.Nts = length(mission.Vert_trg{1});
        
        
        
        
        
end

